# Ali Murteza Yesil - Blog
---

This is my static blog generated using [Pelican SSG](https://blog.getpelican.com/ "Official website") and [blueidea theme](https://github.com/nasskach/pelican-blueidea "GitHub repository"). Pelican Static Site Generator is powered by Python.

> Blueidea theme is licensed under [MIT License](https://github.com/nasskach/pelican-blueidea/blob/master/LICENSE).

All articles and resources used in articles are Licensed under [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/legalcode) License unless otherwise stated.

![CC BY-SA 4.0 badge](https://licensebuttons.net/l/by-sa/4.0/88x31.png)

# Thanks

[Jan T. Sott (idleberg)](https://github.com/idleberg "GitHub profile") for [Creative Commons License markdown file](https://github.com/idleberg/Creative-Commons-Markdown).
[nasskach](https://github.com/nasskach "GitHub profile") for [pelican-blueidea theme](https://github.com/nasskach/pelican-blueidea)

