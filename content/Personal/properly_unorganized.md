title: Properly unorganized
date: 2020-08-24 12:25
tags: 100DaysToOffload
summary: Chaotically Organized File System
status: published
comment: https://fosstodon.org/@murtezayesil/104745430139845480
hundreddaystooffload: 23

I always envied people who can keep their living space tidy, data storage organized and wake up on time. Thanks to living abroad as a student, my life fits into 1 luggage, 1 handbag and 1 backpack. I have to throw stuff around get untidy due to lack of stuff. That being said, I have Juggernaut with enough SSD and 1TB HDD and I heavily utilize the HDD.

I am not going HDDless even on my laptop just yet thanks to some advantages it offers when paired with much faster SSD.

1. Offloading data on SSD to HDD is important for longer SSD lifespan.
	Data blocks on SSD is much more fragile than sectors on HDD. If most of the SSD is filled, smaller section will be used for supervisioning and cause certain blocks to age much faster and possibly causing premature death of SSD.
2. SSD speed can be affected by how much of it is filled. Less data on SSD means the faster it can operate.
	SSD write operation depends on finding healthy blocks and utilizing them instead of previously used and more degraded blocks. More available storage means faster the SSD will find a suitable block and start operation.
3. HDD is my first line of backup and redundancy.
4. Local HDD is faster than remote NAS
5. Local HDD doesn't require a local NAS server.

<div style="text-align: center;">
	<img src="images/screenshots/hdd_storage.webp" alt="File manager showing some directories in Juggernaut's HDD, the storage device. Visible directories are Books, Backups, timeshift, Linux and sub directories of Linux such as Distro Hop Backups">
</div>

You probably got that I heavily use my HDD.
It has an installed distro for recovery (that I hop will never need to boot into), my data from previous distro hops, Steam and GOG backups, game saves, encrypted diary, distro ISOs, ebooks, phone backups etc, etc, etc.

My HDD is a beautiful mess. Even though I organize stuff in outer most level, Not knowing whether something is in Pictures or Backup/[Device]\_[Date]/Pictures or Linux/Distro Hop Backups/[Distro]\_[Date]/Pictures turns looking for files into dumpster diving. And I still can find the stuff I am looking for. __It annoys me but doesn't disable me__ from finding what I need. I just hate myself for being disorganized and envy tidy people.
