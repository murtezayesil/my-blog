title: Blogging vs Microblogging 
date: 2020-12-13
tags: 100DaysToOffload
summary: I've seen many people complain about character limitation on micro-blogging platforms they use. I think 500 characters are enough for micro posts.
status: draft
comment: 
hundreddaystooffload: 

__Blog__: abbreviation for "web log". Blog posts are either written by people about their personal interests, opinions, knowledge, idea etc. or by companies or organizations for them to give news or their positioning about things going on internally or xeternally.

__Micro-blog__: blogging except with micro posts. Posts can be as short as a paragraph, sentence, photo, video, link, abbreviation or even an emoji.

⚠ I will do some napking math and use ASCII for demonstration. I know that many micro-blogging platforms use [Unicode](https://en.wikipedia.org/wiki/Unicode) rather then [ASCII](https://en.wikipedia.org/wiki/ASCII) to support letters from all languages and even emojis. But my points p

I ran a [poll on Fediverse](https://fosstodon.org/@murtezayesil/105367216140737780) a couple days ago asking how long (in terms of character limit) should a post be for it to grow out of being a micro post into a blog post. 18 people responded.

* __280__	0/18 - Twitter default  
* __500__	5/18 - Mastodon default  
* __1000__	7/18 - used by some Pleroma instances  
* __4000__	6/18 - WhatsApp default  

I expected people to NOT like little character limitations but I was't expecting someone reply with "Well above any of those." 😂

I have seen many people complain about the character limitation for micro posts on micro-blogging platforms. You could argue that micro-blogging should make it easier for us to share whatever is in our mind fast and without limitations, but I disagree with the "without limitations" part. I believe that "500 character is (almost always) enough." and this blog post is about my reasoning. Because it wouldn't fit into a micro post 🤣

1. Why we need a limit
2. How restrictive the limit should be

## There should be a fixed length limit on how long a post can be

Some people would like to get rid of the limit all together. This seems like a nice idea to many but it brings huge problems with it.

### Micro-blogging platform without character limit is hard to maintain

Micro-blogging platforms tend to serve to more users and host insanely more posts then blogging platforms. Sustaining performance in the long term is important and a very good excuse to use a well structured database with very fixed rules.

We could create a plain text file for each post and store content and metadata of the post in that text file. But doing so would make maintaining that platform more and more difficult as the number of posts increase. Blog sites, such as this one, can afford to not have a fixated database in favor of __character count freedom__. But micro-blogging platforms will suffer a lot from being disorganized.

### "No Limit" can be abused easily

If you are offering a platform for people to speak, all kinds of people will jump in to use it. Griefers and trolls included. Such people could upload thousands of lines long files as single post and cause server to strugle to keep up, or even come to halt, with little effort.

## 500 characters is the sweet spot IMO

I believe 500 character is many enough to get your point across while being little enough to be managable.

### Micro-blogging platforms are for micro posts

It is in the name dear. If you are triggered after reading a post and want to write an essay to get your point across, write a feature-film long blog post. Then share its URL as a micro post.  
In case you need to say the same things in the future, it will be easier to find and refer to the same blog post.

### Micro-blogging platforms just have too many post for meaningful search

Sometimes we want to find a post we have seen long time ago but don't know exactly when. You think you could use search bar for that but no. Micro-blogging platforms tend to host many more posts then blogs. And sieving the entire database for a specific text is very expensive in terms of computer performance. Starting such a large task could bring everything that server is doing to halt. This is why search bars in micro-blogging platforms tend to be limited to user names, hashtags and your own posts.

Please prefer blog posts over micro posts for better searchability. Because naming micro posts is a pain to and we tend to give them an ID rather then a name. Since blog posts can have names, if you want to find a blog post you read before, you can search for it in your browser history.

### Shorter length post means less storage space needed

Each ASCII character takes 1byte of storage space in a database. By defining post length as 500 characters we assure that each post takes 500bytes ( half KB). If we assume that there is half KB of metadata (post_id, parent_post_id, author_name, publish_date, like_count etc.) we can say that each post takes 1KB space on the database.

If each post is 1,000 characters long, we would need to allocate 1.5KB (including metadata) for each post, or 4.5KB for posts with 4000 character limit.

1KB or 4KB may seem small but it adds up in a system where many people are posting all the time. 1,000 of 1KB is 1MB and 1,000,000 of 1KB would take up 1GB. Now imagine that a micro-blogging platform has 1,000,000 active users who post 10 posts a day. This platform's database would grow by __10GB__ everyday. Here I assumed that the character limit was 500 char/post. But if the limit was 4000 char/post, we would be looking at __4,5KB * 1,000,000 * 10 / 1,000,000 = 45GB__ growth in database everyday.

Less data generated everyday means less headache for admins maintaining those services. It is not uncommon that Fediverse instances shutdown due to difficulty to maintain that instance. 

## Limit can spark creativity

I like short and brief. While I agree that having limitations can reduce your ability to express your idea in its full form, I will argue that the character count limitation can force you to choose your words more carefully. You may need to use your creativity to make the best use of your vocabulary. Trust me, there will be times you will be fine such specific words that explain what you mean very well that you will look like a genious for using that word 😉
