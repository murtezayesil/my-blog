title: Digital Cleansing - Jitsi
date: 2020-07-18 00:00
tags: digitalcleansing, privacy, jitsi, 100DaysToOffload
summary: My family and relatives live different countries and make good use of video calling services regardless of who is offering the service
status: published
comment: https://fosstodon.org/@murtezayesil/104535970036319662
hundreddaystooffload: 5

My family is spread into 3 countries in 3 different continents. If we include my close relatives too, these numbers go even higher. It is important to stay in contact with your family and relatives in Turkish culture and we try to do that. Let it be weekend Zoom meetings (in 40  minute chunks :) or phone calls on WhatsApp (owned by Facebook), we heavily rely on third party services for communication. After launching NextCloud for my family to use, I what I wanted to tackle the __Communication__ problem.

We have 3 kinds of communication needs in the family:  
1. Text messages  
2. Voice Calls  
3. (Mostly group) Video Calls

---
### Text Messaging & Voice Calls
I have been usign Telegram wherever I can for few years. Its UI is very similar to that of WhatsApp which I hope will ease the transition for my relatives. Since it also has voice calling, I don't need to look for another service for that. I love hitting two birds with one stone (only in metaphor) 😄️   
That being said, I won't actually talk much about WhatsApp. Facebook bought WhatsApp back in February 2014. I believe that was a great deal for Facebook and a terrible deal for users.  
I know I mentioned Telegram but there is one more great alternative to WhatsApp (or even Telegram). It is called [Signal](https://signal.org/ "Official page") and it is developed by a non-profit founded by Co-founder of WhatsApp, Brian Acton. It is one freaking secure messaging app 😎️

---
### Group Video Calls
My families' and relatives' current choice of Group Video Calling service is Zoom, just like millions of other people who needed a video calling service for remote work, distance education and calling their loved ones. But Zoom seemingly popped out of nowhere for many people. I wanted to learn more about who Zoom is and how trustable it is. I hope my findings will help you to make educated decisions.

Zoom was [launched in September 2012](https://en.wikipedia.org/wiki/Zoom_(software)#History "History of Zoom on Wikipedia"), reached [1 Million user base in January 2013](https://www.tmcnet.com/topics/articles/2013/05/23/339279-zoom-video-communications-reaches-1-million-participants.htm "Zoom Video Communications Reaches 1 Million Participants - TMCnet") and rapidly grow during global quarantine to a point that Zoom got [2.13 Million downloads on March 23rd 2020](https://web.archive.org/web/20200422125131/https://www.theguardian.com/technology/2020/mar/31/zoom-booms-as-demand-for-video-conferencing-tech-grows-in-coronavirus-outbreak "Zoom booms as demand for video-conferencing tech grows - The Guardian [archive]").

After some research (reading Wikipedia) I found that Zoom had many wounds that hurt many of its users. Given that Zoom reached 1 Million userbase 5 months after launching (from September 2012 to January 2013) and they were charging 9.99$/month, I expect Zoom to invest into infrastructure and app security. I am saying this because they clearly had time to fix issues in their apps before the pandemic arrived. Here are few examples to show how Zoom messed up:  
__Windows__ : [Attackers can use Zoom to steal users’ Windows credentials with no warning - ars technica](https://arstechnica.com/information-technology/2020/04/unpatched-zoom-bug-lets-attackers-steal-windows-credentials-with-no-warning/)  
__MacOS__ : [Zoom Zero-Day Bug Opens Mac Users to Webcam Hijacking - threat post](https://threatpost.com/zoom-zero-day-mac-webcam-hijacking/146317/). This prompted Apple to use its MRT (Malware Removal Tool) to remotely delete Zoom from Mac computers.  
__MacOS__ : [Zoom Zero Day: 4+ Million Webcams & maybe an RCE? Just get them to visit your website! - InfoSec Write-ups](https://medium.com/bugbountywriteup/zoom-zero-day-4-million-webcams-maybe-an-rce-just-get-them-to-visit-your-website-ac75c83f4ef5) allowing Zoom to reinstall itself after being uninstalled and join a video call with camera activated without user's permission.  
__MacOS__ : [Zoom App installation uses the same method used by malwares to gain root priviledges - Twitter thread on Nitter](https://nitter.net/c1truz_/status/1244737672930824193)  
__iOS__ : [Zoom iOS App Sends Data to Facebook Even if You Don’t Have a Facebook Account - Vice](https://www.vice.com/en_ca/article/k7e599/zoom-ios-app-sends-data-to-facebook-even-if-you-dont-have-a-facebook-account)  
__Android__ : I didn't find any news about Zoom Android App vulnerabilities. But if they used Facebook tracker in iOS app, I don't see any reason why zoom wouldn't use the same on Android
__Linux__ : No vulnerability was found YET. Remember that [Linux desktop has a small marketshare](https://netmarketshare.com/operating-system-market-share.aspx?options=%7B%22filter%22%3A%7B%22%24and%22%3A%5B%7B%22deviceType%22%3A%7B%22%24in%22%3A%5B%22Desktop%2Flaptop%22%5D%7D%7D%5D%7D%2C%22dateLabel%22%3A%22Custom%22%2C%22attributes%22%3A%22share%22%2C%22group%22%3A%22platform%22%2C%22sort%22%3A%7B%22share%22%3A-1%7D%2C%22id%22%3A%22platformsDesktop%22%2C%22dateInterval%22%3A%22Monthly%22%2C%22dateStart%22%3A%222019-08%22%2C%22dateEnd%22%3A%222020-06%22%2C%22plotKeys%22%3A%5B%7B%22platform%22%3A%22Linux%22%7D%2C%7B%22platform%22%3A%22Mac%20OS%22%7D%2C%7B%22platform%22%3A%22Chrome%20OS%22%7D%5D%2C%22segments%22%3A%22-1000%22%7D "Less than 4%") and apps for it are less likely to be targeted by hackers.

> "Zoom has just had so many missteps."
> - Patrick Wardle, Jamf

You can read about Zoom's vulnerabilities on MacOS and iOS in detail in [this blog post of Objective-See](https://objective-see.com/blog/blog_0x56.html "The 'S' in Zoom, Stands for Security - Objective-See"). 

These issues were __FIXED__ by Zoom. But Zoom took long time to responde some of the cyber security personel as if it didn't care about the user privacy and security. I only mentioned the vulnerabilities in Zoom's apps. Zoom also [contributed to censorship](https://www.axios.com/zoom-closes-chinese-user-account-tiananmen-square-f218fed1-69af-4bdd-aac4-7eaf67f34084.html "Zoom closed account of U.S.-based Chinese activist “to comply with local law” - Axios") by closing human rights activist Zhou Fengsuo's paid account and closing Social activist Lee Cheuk Yan's account upon China's request.

👉️ Since those vulnerabilities are fixed it should be safe to use Zoom, right?  
Unfortunately, NO. Even if apps became less vulnerable, users still are through weak privacy practices and use of third party trackers. Zoom's Privacy Policy is [not assuring enough](https://zoom.us/privacy#_Toc44414842).

👉️ They introduced end-to-end encryption, E2EE. Is it insecure encryption?  
AES-256 ECB algorithm used for E2EE is one of the greatest encryption algorithms out there. But it isn't enabled by default and enabling E2EE disables many features such as screensharing, which doesn't incentivise people to use E2EE. Both enterprise customers and teachers would want to use screensharing, thus not using E2EE.

👉️ They say Zoom encrypts every meeting by default. Are they lying?  
No, they are not. But they aren't telling the whole story either. When you start a Zoom meeting, your device establishes a connection to Zoom over [HTTPS](https://en.wikipedia.org/wiki/Https#Security). Meaning data is encrypted during transmission between you and Zoom. Data gets decrypted in Zoom and encrypted again before it goes to whoever you are meeting with. This is done because everybody in the meeting has different [session key](https://en.wikipedia.org/wiki/Session_key) for encryption. Your meeting is apparent to Zoom, not hidden from it.

👉️ Zoom has faced [0-day attacks](https://en.wikipedia.org/wiki/Zero-day_(computing) "Learn about zero day attacks on Wikipedia") which weren't fair.  
Not a question but I get your point. When a cyber security personal discovers a vulnerability, (s)he informs the vendor about the vulnerability in disguise and asks for bounty. Vendor checks if that is a legit vulnerability or a scam. Then they work together to fix the issue and vendor pays the bounty. Many companies have a [bug](https://www.intel.com/content/www/us/en/security-center/bug-bounty-program.html "Intel offers upto 100,000$") [bounty](https://hackerone.com/verizonmedia?type=team "Verizon offers upto 15,000$") [program](https://www.microsoft.com/en-us/msrc/bounty "Microsoft offers upto 100,000$").  
A cyber security personal may choose to release the vulnerability to public for it to be exploited by other people, which turns a vulnerability into zero day attack. This action incentivises vendor to fix that issue immediately since issue became well-known.  
Zoom had time since 2013 for testing its softwares properly. Proper testing would uncover those bugs before hackers did. Any company that is careless about security and privacy of its customers' deserves to be pinched to start acting.

What if I am forced to Zoom by my employer/school/family?  
Desktop/laptop users:  
1. Windows: Use virtual machine and apply one of the below Linux methods  
2. MacOS: Use virtual machine and apply one of the below Linux methods  
3. Linux: Install Linux Live image on a USB and boot into it everytime you need to Zoom. Install Zoom into that Live environment. Zoom will only be able to access what is in that Live environment. Shutting down a Live environment deletes everything that was installed in that session.  
4. Linux: [Install Zoom into a firejail](https://ar.al/2020/06/25/how-to-use-the-zoom-malware-safely-on-linux-if-you-absolutely-have-to/ "How to use the Zoom malware safely on Linux if you absolutely have to - Aral Balkan"), greatly limiting what it can reach.  
Mobile users:  
1. Android: [Create a restricted user](https://www.howtogeek.com/333484/how-to-set-up-multiple-user-profiles-on-android/ "This process maybe different for different brands") on your phone and install Zoom there. Not in your main user.
2. Android: If you can, use Zoom on Linux as described above.
3. iOS: Don't give it permissions if you don't need them. Don't let it run in background. Uninstall after using.
4. iOS: If you can, use Zoom on Linux as described above.

PS: I don't own a Mac, iPhone or Windows PC. But since Zoom on those platforms seem to be the affected most, I recommend everybody to use Zoom on Linux in a firejail if you absolutely have to.

I also wanted to read articles [that](https://medium.com/@rowantrollope/beyond-the-noise-7-reasons-its-safe-to-run-zoom-9a2e639b13ec) [defend](https://blog.prialto.com/3-reasons-why-zoom-provides-the-best-video-conferencing-software) [Zoom](https://www.forbes.com/sites/rebeccabellan/2020/03/24/what-you-need-to-know-about-using-zoom/#3cee9d0d3284). But they are mostly talking about Zoom's E2EE feature (that is not default), how people got creative with Virtual Background feature, Zoom's clean UI, ability to fake paying attention and its price. They either say nothing about Zooms privacy policy or even if they say something, it is not assuring in my opinion.

---
# Jitsi

Jitsi is an open source alternative to Video Calling (Conferencing) services. I will prove that Jitsi is much better than Zoom with only 1 sentence.

<h1 style="text-align: center;">You can host Jitsi on your own server without relying on another entity</h1>

You want more?

1. Jitsi has Clean UI that is familiar to that of Zoom.  
2. Jitsi __doesn't__ have looping video feature which helps students or emplyees fake paying attention.  
3. Jitsi is <strong title="Free Open Source Software">FOSS</strong> developed by [8x8](https://8x8.com).  
4. Hosting Jitsi doesn't require a server with powerful CPU or GPU. Important resource is bandwidth.  

 - Jitsi doesn't have virtual background but it instead has background blurring in development.

👉️ Is it truely E2EE?  
__This is what I understood from reading [this threat](https://github.com/jitsi/jitsi-meet/issues/409#issuecomment-260652107). Please correct me if I am wrong__  
Short answer is No.  
Long answer is: Just like in Zoom's case, connection between users and Jitsi VideoBridge (server) is encrypted. Server decrypts and encrypts everybody's stream for everybody else. But by having the control of Jitsi VideoBridge (server) by hosting it on your own server, you can assure that no other company/organization is holding your plain data except the recepient you are meeting with.  
That being said, Jitsi can establish P2P connection in rooms where there are only 2 people. This is a feature of WebRTC that Jitsi is built upon. It still __isn't__ true E2EE.

👉️ Do anyone even use it?  
Glad you ask. Many companies banned use of Zoom and switched to alternatives such as Microsoft Teams, Skype, Hangout Meet and Jitsi. You probably won't see Jitsi's UI very often in the wild, but many companies use Jitsi VideoBridge as their backend for video conferences. Out of all the alternatives, only Jitsi allows self-hosting of server (Jitsi VideoBridge) AFAIK.

## How to install Jitsi server?  
I followed __Nerd on the Street__'s [Host a Jitsi Meet Server](https://invidio.us/watch?v=IQRwtUamHQU) installation tutorial. It took about 30 minute of my time (I am a noob) to get the server running. It takes another 10 minutes to secure it.

---

When I started this blog post, I expected to list 2 reasons not to use WhatsApp and Zoom then start talking about why Jitsi is the answer to my family's Group Video Calling needs. To fact check what I knew about about them (Zoom in particular) I searched them on [Wikipedia](https://en.wikipedia.org/wiki/Zoom_(software) "Read more about Zoom on WikiPedia"). I learned much more than I expected. I am sorry for turning this post into "Rant of Zoom". I hope you learned a thing or two too.

---
### Other side of the coin
If you think I would be better of sticking to WhatsApp or Zoom, tell me more. Even though I read many negative things about Zoom, I will try my best to keep an open mind and hear people seeing other side of the coin. I am a human and can make mistakes. If there is something important I should know to better understand what is going on, please reply to comment toot linked below.
