title: Mezarın Nur, Makamın Cennet Olsun Ömer Hamza
date: 2020-08-21 20:36
tags: Türkçe
summary: Son yolculuğuna uğurladığımız kardeşimiz Muhammed Ömer Hamzaya layık olmayan yazım
status: draft
comment: 
<!-- lang: tr -->

Bugün Müslimanlar için mübarek olan Cuma günü. Güneş en büyük gülemsemesini kuşanmış ve kuşlar cıvıldaşıp günü bayrama çeviriyorlar. Bizim için değil ne yazık ki. Ameliyattan sonra dinlenmeye çekilipte gözlerini Dünyaya bir daha açmayan kardeşimiz __Muhammed Ömer Hamza__nın cenazesine gidiyoruz.

Her taksiye binişimde kulaklıkları takıp birşeyler dinlerim. Bugün farklı. Taksi bizi camiye götürürken ölümü düşünüyorum. Tekerleklerin asfalt üzerinde gıcırdarken içimde ki burukluğa kulak veriyorum. Sanki tabutta ki benim.

Ben derin düşünceler içindeyken vardık camiye. Kalbi burukları takip et ve beyazlara bürünmüş Güzel Muhammedi bulursun. Dikkatimi çekiyor tabutu yok, kovid19 acelesi heralde. Cenaze namazından sonra Görkemli Ömeri cenaze aracına koyup son evine doğru yol alıyoruz. Hıristiyan Müsliman yarı karışık mezarlık. Düz yerler kapılmış yarı yamaç bir mezara indiriliyor Şanlı Hamza. Şükürler olsun onu son yolculuğa uğurlarken azda olsun taşımak, mezarına kum atmak nasip oldu. Okumak için geldiği yurt dışında vefat edince annesi ve babasına nasip olmadı cenazesine katılmak. Onlar içinde birer kepçe kum attım.

Muhammed Ömer Hamza ile bu Dünya da yollarımız ayrıldı. Onu sorgu melekleri ile başbaşa bıraktık. Dualarımda Muhammed Ömer Hamza dahil bütüm sevdiklerim ile Cennette bir araya gelmek ve ailesinin huzur içinde oğullarının mezarı başında Yasin okumaları var.

Ruhun şah  
Mezarın nur  
Makamın cennet  
Başımız sağ olsun  
