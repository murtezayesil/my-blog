title: Swap on SSD done right
date: 2020-08-01 12:00
tags: 100DaysToOffload
summary: SSDs are fragile and swaps can be damaging. But they can live happily together.
status: published
comment: https://fosstodon.org/@murtezayesil/104613505063011977
hundreddaystooffload: 12

TL;DR : If you have SSD and need swap, you should use swapspace daemon for healthier SSD. Btw, you want SSD and you need swap.

Note: I expect you to be familiar with [RAM](https://en.wikipedia.org/wiki/Random-access_memory), [swap](https://en.wikipedia.org/wiki/Paging#Unix_and_Unix-like_systems) and concept of [paging](https://en.wikipedia.org/wiki/Paging).

Here is the short edition:  
__RAM__ is fast memory for currently running programs.  
__Swap__ is kinda like backup RAM. In case of RAM is filling up, swap will be uyilized to move less often accessed data off the RAM.  
__Paging__ is moving data between RAM and swap memory. It occurs when RAM is close to full and needs emptying into the swap or when a data in swap is needed in RAM.

Since swap is located on HDD or SSD, it is much slower than RAM and we want to utilize RAM as much as we can. But sometimes RAM fills up and computer runs into [__OOM__](https://en.wikipedia.org/wiki/Out_of_memory "Out Of Memory") state. Even though it is slow, swap can be a life (or work) saver in such cases. 

### Estimated Memory Speeds

Device Type | Speed (MB/s)
------------|-------------
HDD         | 100        
SSD - SATA  | 560     
SSD - NVMe  | 2000-3000
RAM DDR2    | 3000-8000   
RAM DDR3    | 8000-12000  
RAM DDR4    | 19000-35000 

Data of RAM speeds is from [this article of Crucial](https://www.crucial.com/support/memory-speeds-compatability). HDD and SSD speeds are commonly observed speeds. Not from the Crucial's testing.

You definitely want swap memory to avoid possible OOM issue. You also may want to use SSD for swap memory since it is much faster than HDD or your computer only has SSD. But SSDs have one weak spot. Storage blocks of the SSD that is being read and wrote 100s of times will fail. Thankfully SSDs got smarter. Instead of using a single block too often and causing it to die early, SSDs tend to utilize every block equally. This is called Garbage Collection. And together with [overprovisioning](https://www.invidio.us/watch?v=Q15wN8JC2L4 "Techquickie video on overprovisioning"), they increase longevity of the SSD.

I don't think fixed swap partitions and files are good for SSD's health. We need a system to dynamically allocate swap. So that swap file will be erased when notneeded and created in lesser degraded blocks when needed.

# Meet Swapspace

[Swapspace](http://manpages.org/swapspace/8 "read manpage") is a background service that understands swap needs and either creates or removes swap files on the fly.

Swapspace is available on many _local repos_, _AUR_ and on [_GitHub_](https://github.com/Tookmund/Swapspace). 

---

__Note__: How aggressively the Kernel should use swap memory can be controlled via a value called swappiness. Read about [changing swappiness](https://www.howtogeek.com/449691/what-is-swapiness-on-linux-and-how-to-change-it/) for better RAM utilization.

