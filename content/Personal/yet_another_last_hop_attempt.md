title: Yet Another Last Hop Attempt
date: 2020-08-09 22:00
tags: distros, 100DaysToOffload
summary: Solus 4.1 was one of the best polished experiences I had. I hope it continues that way and I don't feel the need for a hop.
status: published
comment: https://fosstodon.org/@murtezayesil/104661159514521536
hundreddaystooffload: 16

I tried many distros ever since I purchased this laptop, Lenovo Ideapad 110, back in 2017. My previous post was about [my Linux journey](my-linux-journey.html). And as it could be seen there, my every hop was due to some frustration or some feature I missed. But leaving a well polished OS like Manjaro and Pop\_OS! is much more difficult and it hurts to need moving away. I tried Pop\_OS! twice and n both cases it slowed within a month. In fact, I have been having problems and hopping once a month pretty regularly.

<div style="text-align:center;">
	<img src="images/distro_backups.png" alt="A directory with backups of important files before I replace the distro on SSD. I put the distro name and the date it was removed on the directory. It turns out I have been distro hopping once a month from February to August of 2020">
</div>

I have been distro hopping every month for the last seven months. I am not some adventurous person who hops for fun, except that time I decided to use [LFS 😅️](http://www.linuxfromscratch.org/ "Linux From Scratch") I heard that people were using Pop_OS! for months with great performance. I used Pop before and experienced performance degragation on SSD. Manjaro stayed fast but installing too many AUR packages got the best of it. Arch, well, was born dead in the hands of a newbie like me 😜️ Elementary is beatiful and good for people switching from Mac, but isn't for me.

Everytime I distro hop, I want it to be the last hopping. I want to settle with a distro on the bare metal. I am not against trying new distros on VM. I am talking about desktop distros btw. Since I am new to server space, I only tried Ubuntu 20.04 and Debian 10 so far.

# Solus 4.1 vs Solus 3.99
The biggest improvement I noticed so far is the black screen issue upon booting after a gpu driver upgrade is gone. I hope it stays that way. Another thing I noticed is that my GPU supports Vulkan now. It didn't before. Even though I installed bunch of drivers and libraries from different PPAs as instructed in Lutris and Steam wikis. I simply installed Steam and Solus pulled every Vulkan stuff games would need.

# I want this to be the last hop I will do on baremetal
Be ready to quote me on that as "Last Famous Words"

Fun Fact: Solus' package manager, eopkg, is derived from [Pardus](https://en.wikipedia.org/wiki/Pardus_(operating_system))' package manager [PiSi](https://en.wikipedia.org/wiki/Pardus_(operating_system)#PiSi_package_management "means "kitty" in Turkish") which was the first Linux distro and package manager I used. Unfortunately PiSi was discontinued when Pardus moved to Debian base. It lives in Solus. That is a nice nostalgia.

<div style="text-align:center;">
	<img src="images/neofetch_juggernaut_lenovo_ip110.png" alt="Neofetch command showing system info on the terminal: Username: Murteza, Computername: Juggernaut, OS: Solus, Kernel: Linux 5.6.19, Computer Make: Lenovo Ideapad 110-15ACL, DE: Budgie, CPU: AMD A8 7410, GPU: AMD Radeon R5, RAM: 6860MiB total">
</div>

<div style="text-align:center;">
	<img src="images/juggernaut_screenshot_sunday.png" alt="Screenshot of a clean desktop with no icons. Wallpaper is blurry dark and darker blue with Solus written in the middle. Taskbar is tranparent background with application menu and icons on the left, clock in the middle and system tray for wifi, battery, volume and more indicators on the right" width=100% height=auto>
</div>
