title: I Love Fediverse 😀️
date: 2020-12-31 23:59
tags: 100DaysToOffload
summary: 
status: draft
comment: https://fosstodon.org/@murtezayesil/
hundreddaystooffload: 

You should know by now that

1. Fediverse is decentralized. There are many small servers communicating with each other. They collectively form Fediverse.

2. Fediverse is formed by many instances running as different platforms. Therefore Fediverse is many platforms.

Some instance (running Mastodon or Pleroma) is a microblogging platform, other instance running PeerTube is a video sharing platform, another instance running PixelFed is an image sharing platform and some other instance running Funkwhale is a music sharing/streaming platform. Someone on Mastodon can like a music on Funkwhale and comment (aka toot) to a photo on PixelFed. You can comment to a video on PeerTube with your PixelFed account and that video creator can reply to your comment and you would get a notification on PixelFed.
It isn't a platform that is only for one thing. It is the combination of many different platforms. There are many servers running different services and all connect to each other. Together they form Fediverse. We call those servers, instances. Some servers run Mastodon or Pleroma instance to offer microblogging 
