title: Decentralized Internet is More Reliable
date: 2020-07-28 12:00
tags: 100DaysToOffload
summary: Every system is prone to failure and will face down time. Decentralization avoids total system failure.
status: published
comment: https://fosstodon.org/@murtezayesil/104591271530394013
hundreddaystooffload: 10

# Introduction
I was looking for a way to explain decentralized internet to my family. Then Yarmo Mackenbach came up with the idea of [ELIUF](https://yarmo.eu/post/eliuf "Explain Like I Use Facebook"). A decentralized attempt to explain concept, advantages and disadvantages of decentralized internet to people using centralized platforms, such as Facebook, Twitter, YouTube, Instagram, TikTok, WhatsApp, etc.

From power surge to aged hardware, bug in software to cyber attack, there are many reasons a system may fail. Neither centralized or decentralized systems are %100 safe from failures.

---

# System failure

## Centralized Systems

There is a central server that stores every data required to offer a service. Any failure in the central server may cause it to go out of service. Any maintenace such as updating software or replacing aged hardware may require powering server off thus putting it out of service. 

## Decentralized Systems

In a decentralized network, there are multiple servers and each of them store only the data for their own clients. Servers communicate with each other if they need to get data stored in some other server. If any server fails or goes to maintenance, only the clients connected to that server will lose service. Rest of the network will continue to function as usual. That being said, any attempts to communicate with failed server will fail too.

---

# Cyber attacks

User credentials (such as emails, passwords, bank card numbers) and user data (such as profile pictures, birthday videos, phone numbers) uploaded to server are stored by the server and they make an attractive target for cyber criminals.

## Centralized Systems
All the data is available from central server. Cyber attacks put every single user's data in danger.

## Decentralized Systems
There is no server that has access to every data of every user. When servers are communicating, they only share minimum amount of data required. User credentials (email and password) are only known by the server user is connected to. If a server is attacked by cyber criminals, only the users of that server will get affected. Users on the other servers of the network won't be affected by the attack. That being said, all the communications made to attacked server may also be visible to attackers.

---

### Redundancy isn't decentralization

Both centralized and decentralized platfroms may utilize redundancy servers to avoid going entirely out of service in case of server failure. Facebook for instance deploys multiple data centers in different countries. So if one of the data center goes to maintenance, users connected to it will be routed to other data centers and continue to receive the service without down time.  
Redundancy is a good way to ensure reliable network but also costly one.

---

# Conclusion

Decentralization doesn't make a server more durable or secure. Every server, regardless of whether a part of centralized or decentralized network, is prone to failures and cyber attacks. Decentralization reduces the impact and limits the number of users being affected.

---

## ELIUF posts elsewhere on the internet

- [ELIUF: Explain Like I Use Facebook](https://yarmo.eu/post/eliuf) by [Yarmo Mackenbach](https://yarmo.eu/)
- [How to decentralize the Internet](https://www.garron.blog/posts/eliuf.html) by [Guillermo Garron](https://www.garron.blog/about.html)
