title: Privacy For The Whole Family
date: 2020-07-10 11:18
tags: privacy, nextcloud, self-hosting, 100DaysToOffload
summary: My story of learning about wounds in my privacy and my first steps to cure it, helping my family for the same too.
status: published
comment: https://fosstodon.org/@murtezayesil/104497196983720440
hundreddaystooffload: 1

According to my mother, we had internet in our house while I was a baby. Internet back in the day used to make iconic dial sound, was slow and would lose connection whenever someone called the landline. I grew up seeing webpages full of GIFs (I won't argue about its pronounciation, it was decided long ago) and banner ads injected by adwares. Flash and Shockwave were the fundamental building blocks of interactive webpages with animations.

All those colorful flash games sites were offering tones of free games in exchange of distributing adverts and malware in their websites. As a child attracted by colors, I believed that those games were actually free. Some of those "free" game sites are still around and powered by Google AdSense. Others moved to Facebook and started earning from Facebook Ads and in-game currencies often called "gems". Around that time my classmates started to talk about Facebook and I had to go there too.

I grew up since then. I came abroad to study computer science. I have my own laptop and smartphone. I was using my devices and all those free services to talk to my family from thousands of kilometres away. I was using those sweet and free services for backing up my data, photos, documents, contacts and more. These services are free for us because all those wonderful advertisers are paying them off their goodwill ...

#### *__So I thought__*

I was naive to upload my and my family's photos to Google Photos. I was naive to tag my friends in photos on Facebook. I was naive to use Amazon instead of taking a walk in tech market and support the independent sellers. I was feeding tech giants for convenience and damaging local economy without knowing. I didn't know any better and I confussed giving up my privacy with convenience. I am not that naive kid anymore. I learned English Language in Kenya and I learned to harness the knowledge in the internet. I learned that there are alternatives that I can use.

#### *__I learned that there is a way to gain my digital freedom__*

My first action was to switch to [![DuckDuckGo logo](https://fosstodon.b-cdn.net/custom_emojis/images/000/010/368/static/duckduckgo.png) DuckDuckGo](https://duckduckgo.com) from Google Search. I found that the most widely adopted service of Google is a text box for us to write our most intimate secrets in plain text.

![Google wants user data. Users use Google Search for sensitive personal issues. Google says "It's Free Real Estate"]({static}/images/it_is_free_real_estate_1.jpg)

Dropping Google Search wasn't gonna cut it though. I still was relying on Google Contacts, Photos and Drive to backup my data. I needed something that could backup my phone properly while not giving up my data to data hungry companies.

# NextCloud to the rescue

NextCloud is a file hosting service with built-in CardDAV (contact sync), CalDAV (calendar sync) and WebDAV (file sync) servers. Not to mention, it supports adding more features by installing modules from its [apps library](https://apps.nextcloud.com/). It is [FOSS](https://en.wikipedia.org/wiki/Free_and_open-source_software "Free and Open Source Software") free as in freedom for everyone and int this case free as in price for non-enterprise users. Nice thing about NextCloud is that it offers _all of its features_ to both its enterprise and home/personal users. Enterprise users also benefit from technical maintenance support direct from the NextCloud.

The way I deployed my NextCloud instance was to rent a remote VM. I am a student who can't afford to buy a machine and run it 24/7. Renting a server seemed like the most affordable and logical idea to me. Rent costs $5/month for the cheapest tier. I am careful with my pocket money and after cancelling my Netflix subscription, I had more than enough to pay $5/month.

After renting a server, I started experimenting with different OSes. I tried Debian but its php packages were old. I tried CentOS based NethServer but default user credentials for NextCloud were wrong and I couldn't use it either. I finally settled at Ubuntu 20.04 LTS. I followed few tutorials before I found Kev's tutorial. I deviated from those tutorials because I am young and ... nevermind. I finally found Kev's tutorial and 

There still was a problem though. I am not he only one sharing data about me. My family can do that too. And I felt responsible for helping my family with claiming their digital freedom too. I decided to create accounts for them and help them migrate to my NextCloud instance. It turns out my family acknowledged the privacy they were giving up for the convenience. But they didn't know any alternatives. When I invited them to use my NextCloud instance they were excited. But moving from Google Suite to NextCloud isn't done yet. It takes time to learn new systems and their quirks.

## Conclusion

I think this was a successful attempt. Of course I am not done here. I may need to introduce my family to Fediverse, free social media powered by voluntaries, or Jitsi, Open Source alternative to Zoom and WhatsApp video calls services. But for now we all made good progress I would say. I will let some time pass until they get more comfortable at using NextCloud. I don't want to overwhelm them and make them regret switching. I don't want to be another reason they stay on Google. 

Meanwhile, why won't you make an attempt to gain your own digital freedom ?  
I see many ways this can be achieved (numbers in front suggest how difficult I think they are, rated out of 5):  
3 - Manually install NextCloud on a remote VM  
2 - Install NextCloud via Snap Package on a remote VM  
4 - Manually install NextCloud on an old computer  
3 - Install NextCloud via Snap on an old computer  
1 - Purchase a [Synology](https://www.synology.com) NAS to easily self-host data backup, sync and similar services  
2 - Purchase a shared hosting on one of many service providers tested by NextCloud company

## Tutorials

__Installing on own server__: Kev beautifully explains both Snap Package way and manual way of installing in his [tutorial](https://kevq.uk/how-to-setup-a-nextcloud-server-in-ubuntu/).  
__Installing on shared hosting__: But if you are intimidated with the idea of setting up your own server and managing it, you can use a shared hosting instead. Kev has a [tutorial](https://kevq.uk/how-to-install-nextcloud-on-shared-hosting/) for that too, albeit it aged a bit old.   
__Repurposing old hardware__: If you have an old computer gathering dust in a closet, why not use it as a home server! You will find many tutorials on the internet on [how to install Ubuntu server](https://lbry.tv/@TheLinuxGuy:d/How-to-Install-Ubuntu-Server-20.04-LTS:5). 
---
The only reason I followed manual method was some rookie mistake I made that caused Let's Encrypt HTTPS script to not work properly 😬️ I highly doubt you will face the same issue if you follow the tutorials carefully. Even if you do face some problems with [getting HTTPS certificate using `certbot`](https://certbot.eff.org/) command isn't difficult at all.
