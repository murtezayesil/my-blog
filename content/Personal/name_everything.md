title: Name Everything
date: 2020-08-17 17:14
tags: 100DaysToOffload
summary: Finding a suitable name for stuff is both fun and a possible cause of cancer. So, I am naming devices I own.
status: published
comment: https://fosstodon.org/@murtezayesil/104706075280288531
hundreddaystooffload: 20

You probably have a phone and a laptop and possibly even a desktop for heavy duty jobs such as wm ricing. But what do you call those devices? If you have 2 phones how do you differentiate them? If they are the same make, do you use their modal numbers?

I wanted to solve this non-existing problem for my devices so that when I want to refer to any of them in my blog posts, readers would have a confusing (and in my opinion funny) moment and go "What the heck even is X?" in their head 😜️

# [Juggernaut](https://www.thefreedictionary.com/juggernaut "An overwhelming or unstoppable force")
My laptop which was low end back in 2017 when I bought it brand new and even lower end now 7nm era.

Model: Lenovo IdeaPad 110 15acl  
CPU: AMD A8 7410 - 4 cores each ticking at 2.2GHz  
iGPU: AMD R5 - 900MHz 1GB RAM from shared memory  
dGPU: AMD R5 - 1050MHz 2GB RAM  
RAM: 8GB DDR3 1600MHz  
SSD: 240GB with DRAM - Solus OS + swap  
HDD: 1TB 5400rpm - Solus OS (for emergency) + Storage

# [Andromeda](https://en.wikipedia.org/wiki/Andromeda_Galaxy "is our neighboring galaxy")
My Android smartphone which is a mid range phone that still rocks after 3 years 👍️  
Screen is still healthy even though both its glass surface and protective glass took a good beating thanks to clumsy me. Front speaker and microphone connection of headphone jack stopped working after a recent drop 😐️ I still like this phone and looking forward to using it for another 3 years.

Model: Redmi Note 4 - mido  
CPU: SnapDragon 625  
RAM: 4GB  
Storage: 64GB  
Battery: ~3000mAh (was 4100mAh but aged 25% in 3 years)

# Sophisticated Noise Cans

Model:Redmi Airbuds in-ear Bluetooth earphones

I bought them after dropping phone which caused front speaker to brake and headphone jack's metal piece for microphone come out. I only have it for phone/video calls and it was the only solution that fit into my budget. Without them the only other way to make phone calls is to use loudspeaker which is not always appropriate. Sound quality is good for the price (20$).

What I don't like about Airbuds is the in-ear design. I tried tips with different sizes but my ears still hurt. Not only that they hurt which I can get used to, using in-ear earphones feels like my ears are blocked.

---

Do you name your devices? What are the creative names you came up for your devices?
