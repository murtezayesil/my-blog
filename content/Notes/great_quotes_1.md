title:   Great Quotes #1
date:    2020-12-15
status:  published
summary: Jaron Lanier's quote at one of his TED Talks
tags:    100DaysToOffload
comment: https://fosstodon.org/@murtezayesil/105385407518555166
hundreddaystooffload: 33

> We cannot have a society in which, if two people wish to communicate, the only way that can happen is if it's financed by a third person who wishes to manipulate them.
>
> Jaron Lanier, [How we need to remake the intenet](https://www.ted.com/talks/jaron_lanier_how_we_need_to_remake_the_internet), TED2018
