title: $ADB_VENDOR_KEYS is not set
date: 2020-07-22
tags: android, problem, 100DaysToOffload
summary: Authorizing adb when no prompt will come, the hard way. 
status: published
comment: https://fosstodon.org/@murtezayesil/104558575376752243
hundreddaystooffload: 7

## Story
I like [scrcpy](https://github.com/Genymobile/scrcpy "scrcpy by Genymobile - GitHub"). It allows me to cast my phone to computer and use it ther thanks to cable technology (wifi works too). So, after hopping to <img src="https://fosstodon.b-cdn.net/custom_emojis/images/000/025/123/static/a76f54a7dbcd8afd.png" width="15em" height="15em"> Pop_OS! I reinstalled adb and scrcpy.
But this time scrcpy didn't work. Instead it throw this error:  
<blockquote style="font-style: normal;">
<code>
adb: error: failed to get feature set: device unauthorized.  
This adb server's $ADB_VENDOR_KEYS is not set  
Try 'adb kill-server' if that seems wrong.  
Otherwise check for a confirmation dialog on your device.  
ERROR: "adb push" returned with value 1
</code>
</blockquote>  

## Easy way | Grant access using prompt on the phone
After enabling [USB debugging in Developer options](https://www.howtogeek.com/129728/how-to-access-the-developer-options-menu-and-enable-usb-debugging-on-android-4.2/) connect phone to computer and run `scrcpy`. Phone should prompt you for granting access to computer. All you have to do is accept and connection will be granted 🎉️

---

This prompt didn't appear on my phone. As it turns out, that prompt doesn't works as expected on some custom ROMS. After some digging online, I came across [this answer on stack __overflow__](https://stackoverflow.com/a/26309956). Which leads to a [commit message for (new) adb authentication](https://android.googlesource.com/platform/system/core/+/d5fcafaf41f8ec90986c813f75ec78402096af2d) implemented in 2012. This prompt copies ~/.android/adbkey.pub on computer, `$ADB_VENDOR_KEYS`, to `/data/misc/adb/adb_keys` file in phone's memory. since prompt isn't working for me I have to add that key manually 😕️

## Littler harder way | copy-paste using Termux + root
Since `/data/misc/adb/adb_keys` file is offlimits for the user, we need root priviledges for this method.

1. Copy ~/.android/adbkey.pub to phone's memory
2. Open Termux and install sudo > `pkg install tsu`
3. Navigate to where file adbkey.pub is. I'm using Redmi Note 4 and its `/sdcard` directory is used for .  
File is at `/sdcard/adbkey.pub` in my case > `cd /sdcard`
4. Copy `adbkey.pub` to `/data/misc/adb/adb_keys` with sudo priviledges > `sudo cp adbkey.pub /data/misc/adb/adb_keys` 
5. Double check if content of `/data/misc/adb/adb_keys` on phone and `~/.android/adbkey.pub` on computer are the same
6. Enjoy 😊️

<video width="720" height="auto" controls="controls">
  <source src="https://murtezayesil.me/videos/copying_adb_vendor_keys_using_termux_and_root.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 

---

## Hard way | copy-paste using TWRP
This is the "no-root" hard method. You will still need root priviledges to edit `/data/misc/adb/adb_keys`. If your phone isn't rooted but has TWRP, GREAT. TWRP has root priviledges and a file manager.

1. Boot into TWRP by holding Vol+ and Power keys while the phone is turned off.
2. Connect phone to computer
3. Copy ~/.android/adbkey.pub to phone's memory
3. Go to Advanced > File manager
4. Navigate to where adbkey.pub was copied to. I am using Redmi Note 4 and MTP connects to `/sdcard` on this device.
5. Copy `adbkey.pub` to `/data/misc/adb/adb_keys`
6. Delete `adb_keys` in 
7. Rename `adbkey.pub` to `abd_keys`
8. Double check if content of `/data/misc/adb/adb_keys` on phone and `~/.android/adbkey.pub` on computer are the same
9. Enjoy 😊️

![SCRCPY is casting Android phone's screen on to computer after manually copying computer's adbkey.pub file to phone's adb_keys file](images/scrcpy_casting_android_to_computer.png "scrcpy is finally working")

---

It was a little annoying adventure for me to find where the adbkey.pub suppose to go. I am not immitated with having to do that thing manually. I was annoyed that this information isn't out there in Android documentations and requires reading answers from 2014 on stack __overflow__.

### I hope this guide was helpful to you 😊️
