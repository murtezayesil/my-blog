title: Switching to Bluetooth earphones
date: 2020-08-11 18:29
tags: 100DaysToOffload
summary: After I droped my phone and broke its headphone jack, I had to switch to bluetooth earphone. Redmi Airdots is my choice thanks to its price.
status: published
comment: https://fosstodon.org/@murtezayesil/104672484458036109
hundreddaystooffload: 17

My one and only phone, Andromeda, is a Redmi Note 4. I purchased it 3 years ago. It initially suffered from problems such as consuming battery for self overheating. But I later switched custom ROMs, Lineage and Havoc OS, and battery started to last more than a day. Andromeda served me well in its 3 years of service and it continues to do so albeit with some hickups.

I am not the best phone user. I actually am a clumsy person who drops his phone on regular basis. So much so that Andromeda's display got replaced once and glass twice. Its glass is broken as I write these lines 😁️

But broken glass isn't the issue. It still detects my touches fairly accurately. Real problem is that, a recent drop resulted __front speaker to stop working__ so that I can't hear other person during phone calls and some copper piece come out of headphone jack which rendered __microphone on headsets to not connect__ so that other person cannot hear me during a phone call (if I am using a headphone). My temporary solution was to use loud speaker which is annoying and makes private calls difficult. Also it is rude in public. Another option was bluetooth speaker which requires some budget.

I could spare 20$ for a pair bluetooth earphones. Pretty generous huh 😜️  
Anyway, after some market search I set my mind on Redmi Airdots.

__Do I recommend them?__  
No and Yes. They aren't the best earphones, not even best in-ear earphones, but they are affordable.  
Just like every other in-ear earphones, they aren't comfortable even after I replaced the tips with different size that better fits 😕️ They will do for now. It isn't too much of a deal since I am planning to use them only for calls. Speakers on wired earphones still work with the phone fortunately.

<div style="text-align: center;">
	<img src="images/memes/modern_problems_bluetooth_earphones.jpg" alt="Top Caption: Switched to Bluetooth earphones after phone's headphone jack. Bottom Caption: Modern problems require modern solutions">
</div>
