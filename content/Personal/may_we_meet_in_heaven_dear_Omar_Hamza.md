title: May we meet in Heaven Dear Omar Hamza
date: 2020-08-21 20:36
tags: 100DaysToOffload
summary: My unworthy writing about recently deceased friend, Muhammed Omer Hamza.
status: published
comment: https://fosstodon.org/@murtezayesil/104732050982524858
hundreddaystooffload: 22

Today is Friday, holy day for Muslims. Sun put on its brightest smile and birds are chirping to turn the day into festive. Unfortunately, not for us. We are going to funeral for our beloved friend __Muhammad Omar Hamza__ who closed his eyes to get some rest during recover after his surgery never to open them again.

I always use my headphones to listen to something while riding a taxi. Today is different. I am thinking of death as we ride to the mosque. I am listening to the bitter feelings as tiers grind over the asphalt. Feeling, as if I am the one in the coffin.

While I am in deep thoughts, we arrived at the mosque. Follow the broken hearts and you shall find the Beautiful Muhammad in whites. It grabbed my attention that he isn't in a coffin. Must be the rush during Covid19. After the prayer, we carried the Glorious Omar to hearse to deliver him to his final home, semi-mixed graveyard for Christians and Muslims. Flat places were taken. Great Hamza is lowered to a grave on slope. Thank god I was lucky enough to carry him to the hearse and put dirt on his grave as we farewell him to his final home. He past away in the abroad country he came to study. His parents couldn't attend to his funeral, so I put a shovel of dirt for each of his parents too.

We left him in his final home in this world, along with angels to question him and ask about his deeds. My prayers are that I can reunite with all the people I love, that includes Muhammad Omar Hamza, and his parents can visit the grave of their son and recite Quran.
