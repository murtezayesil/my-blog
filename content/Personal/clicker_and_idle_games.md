title: Clicker & Idle Games
date: 2020-09-22 18:23
tags: 100DaysToOffload
summary: aka Getting to Numbers Which Are Too Large To Read Simulator
status: published
comment: https://fosstodon.org/@murtezayesil/104909697380867713
hundreddaystooffload: 28

I remember the old days of the internet when it had nothing but Flash games (for a child). All the games one can play were behind a advertisement pop-up. There were few Flash games that defined what I pay to play today. But there are 2 genres that I grow to dislike, clicker & idle games.

# How to play
There are 3 main things that will grab your attention very fast. First thing is the big __button__ in the middle. Clicking this button will increase your __points__. Aim of the game is to get very large points which is usually displayed as the big number above the button. But clicking gives very little points and you will need to spend your points to purchase __upgrades__. Upgrades basically are multipliers that makes your clicks worth more points.

# Clicker Games
There is no winning or losing factor in those games. You click the button, buy upgrades, repeat patiently while saving for the final upgrade. Other than final upgrade, there isn't a goal that you can reach. Last time I check the numbers didn't had an end. Or purchasing the most expensive upgrade is the goal.

First clicker game that I played was [💩️ Poop Clicker](https://poopclicker.com/ "It may not run because it is a Flash game and it requires Adobe Flash. Duh"). Its fun was in its silliness. If clicking on a poop emoji from pre-emoji era until buying __The Earth__ upgrade is your jam then this game is your gold mine.

Second clicker game was the delicious [🍪️ Cookie Clicker](https://cookiesclicker.net/ "Wow, there is a no-Flash version. Interesting"). Unlike Poop clicker, cookie clicker had a special place. I used to continue imagining how happy I would be if we actually had that much cookie even after stop playing.

# Idle games
Clicker games are the ancestors of Idle games. Or Idle games are the slightly modified Clicker games that encourage watching advertisements or purchase in-game currency with real life money. It is possible to purchase the final upgrade in an old-school clicker game within 2 hours. Idle games are made to get you to keep coming back. 

They usually achieve getting players to open game repeatedly by giving player short time goals that can be achieved more easily if player opens the game daily, or even every 8 or 12 hours in some cases. In mathematical terms, exponential growth is smaller and upgrades are more expensive, thus requiring both playing for much longer time and opening the game often to purchase upgrades more often.

The ways the developer of an Idle game makes money are income from advertisements and players purchasing in-game currency for bigger multiplier upgrades. Idle games usually implement temporarily __boost__ing idle score gain by watching advertisements.

Idle games are available as mobile apps for you to open them easily from anywhere and not only from your computer with now obsolete Flash. Having them as an app in your phone can be privacy and security hazard too since many of those games are [more](https://reports.exodus-privacy.eu.org/en/reports/com.kongregate.mobile.adventurecapitalist.google/latest/) [trackers](https://reports.exodus-privacy.eu.org/en/reports/com.codigames.idle.game.tycoon.life.sims/latest/) [and](https://reports.exodus-privacy.eu.org/en/reports/com.money.tycoon.idle.train/latest/) [unnecessary](https://reports.exodus-privacy.eu.org/en/reports/com.gamemaker5.idlemafia/latest/) [permissions](https://reports.exodus-privacy.eu.org/en/reports/com.mrdgame.mrd/latest/) [then](https://reports.exodus-privacy.eu.org/en/reports/com.codigames.idle.police.department.tycoon.cop/latest/) [fun](https://reports.exodus-privacy.eu.org/en/reports/com.fluffyfairygames.idleminertycoon/latest/).

# Silly gone, Copycat came
Poop clicker. Doesn't it sound silly? Old clicker games had that funny and ridiculous artwork that made then good memories. Today's implementation is copying what was good to make money. It turned into "easy money" route that some apps developers take by hooking people into a never ending circle. It disgusts me 🤮️
