# Theme 

1. I don't use disqus. Instead I have a toot for each blog post which is treated as that post's comment thread. Adding a link to `comment` in frontmatter suggests readers to toot on thread to comment.

2. I am participating in #100DaysToOffload thingy. Adding `hundreddaystooffload` with a number in frontmatter will add `Day X of #100DaysToOffload` message to article_infos and at the end of the post
