title: Silent Rage Quits
date: 2020-07-30 16:49
tags: 100DaysToOffload
summary: nobody saw it coming
status: published
comment: https://fosstodon.org/@murtezayesil/104603241371480068
hundreddaystooffload: 11

Has it ever happened to you that when you start to change your life for better by building a good habit but at some point you give up seemingly out of nowhere, kinda like rage quit but without rage?

Announcing your goal to public or close friends and family members for them to keep you reminded of your goal is called "motivational accountability". It sounds like a great idea. But it only back fired in my cases.

I decided to announce what my goal was and wanted people to motivate me by reminding me my goals. I got the support of people I love. I also got bombarded with what I should and shouldn't do. I am mad at myself for pushing myself to the spotlight and too much attention.

If you look from outside there was nothing wrong, my path was wide open, weather was clear. People around me made sure of that. I can't be mad at them for their help. I pressured myself by taking too much support which turned into pressure. There is a fine line between supporting someone by reminding one's goals and pressuring them to reach their goals. Even though some people were keeping the balance when mixed with other's pressure, they all turned into pressure.

## I finally snapped and I silently rage quitted

I still do set targets for myself but I don't announce them. They instead are my secrets until they are reached.  
Asking for help, opinions and experiences of others is important for making educated decisions. But it sure is difficult without revealing anything. If people really have to know, it should be as few people as possible in my opinion.

---

### Read on your own risk

In case you are wondering what I rage quitted, it is computer programming (or at least CS degree). After spending 4 years in 2 different universities in 2 different countries and hearing about what programming languages to learn, which online courses to take, what programs to write, how much mark to get, how many hours to sit on the arse and practise coding, practising for interviews, solving puzzles on Leetcode, memorizing a tone of formulas for no other reason than they will be asked in the exam, what should one aim for ...

Maybe you think I got it all wrong and annoyed and frustrated for no good reason. If you don't recall this feeling, I am happy for you. I hope you never experience it.

---

If you find accountability very motivational and working for you then I am glad for you. Let the world know and help you to reach your goals.
