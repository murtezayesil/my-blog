title: Give me an advice on anything
date: 2020-09-16 17:57
tags: 100DaysToOffload
summary: I want to avoid saying "I wish". Give me an advice on anything.
status: published
comment: https://fosstodon.org/@murtezayesil/104874449294243772
hundreddaystooffload: 27

We all say "I wish I did X instead of Y". This is another way of saying "If I knew [something] that I would do things differently". Things that make us say "I wish" are called experiences. We can't know what future holds but we can learn from each other experiences. I want to avoid saying "I wish". Give me an advice on __anything__. Topics can range between The Sun and Pluto 😀️

Please share your advice by tooting a reply to [this toot](https://fosstodon.org/@murtezayesil/104874449294243772). It can be a single word, blog link, poem, picture, computer code, song name and artist, audio and even etcetera 😅️
