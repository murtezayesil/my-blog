title: Kinder on the internet
date: 2020-09-07 23:34
tags: 100DaysToOffload
summary: I heard that we are kinder on social media. What do you think?
status: published
comment: https://fosstodon.org/@murtezayesil/104824883656788953
hundreddaystooffload: 26

I heard that we tend to be kinder to stranger in social media. I believe it is true. When we are meeting strangers maybe in the street or while shopping we either already heard or seen them which is all we need to develop prejudgement thanks to our biases.

What do you think?

<iframe src="https://fosstodon.org/@murtezayesil/104824883656788953/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="760" allowfullscreen="allowfullscreen"></iframe><script src="https://fosstodon.org/embed.js" async="async"></script>
