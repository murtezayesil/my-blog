title: Digital Cleansing - NextCloud
date: 2020-07-16 10:00
tags: digitalcleansing, privacy, nextcloud, 100DaysToOffload
summary: NextCloud has 4 things going for me. It is FOSS, it gives me control, it is convenient and it works.
status: published
comment: https://fosstodon.org/@murtezayesil/104521563799892039
hundreddaystooffload: 4

This article reflects my opinions and experiences with few file server services.

TL;DR : I think NextCloud is a far superior product for the price.

Digital cleansing is about reowning personal data and regaining control over how it is processed. When I started digital cleansing, I wanted to start from where the most of my data is stored. There are 2 such services, Google Drive and Photos. I started by looking for [alternatives](https://alternativeto.net/software/google-drive/). OwnCloud and NextCloud seemed like __affordable__ and <strong title="Free Open Source Software">FOSS</strong> alternatives that allow <strong title="Can be hosted on personal (or home) computer/server without relying on another service provider">self-hosting</strong>.

---
## Owncloud
I started my journey by renting a VM on Digital Ocean, droplet. I installed [LAMP stack](https://en.wikipedia.org/wiki/LAMP_(software_bundle) "Minimum set of softwares needed for a working web service") and [OwnCloud](https://en.wikipedia.org/wiki/OwnCloud "File server service"). As a new comer to OwnCloud, I started to click every button in every menu to discover and learn more about OwnCloud. [Marketplace](https://marketplace.owncloud.com/), a feature manager to add/remove more features, has many stuff that can appeal to enterprises and teams working from home. Next, I browsed the [available Android apps for OwnCloud](https://search.f-droid.org/?q=owncloud "Apps for OwnCloud on F-droid"). To my surprise, there aren't many. I expected niche apps on Android for using niche features on marketplace. Instead, I would run into more [apps branded for NextCloud](https://search.f-droid.org/?q=nextcloud "Apps for NextCloud on F-droid"). Meanwhile I updated the droplet, because updates are important, but ran into "kernel updates rendering server unbootable" kind of issues, I switched to Linode and NextCloud after strugling on Digital Ocean for a week.

Just like Owncloud's marketplace, NextCloud has its own "app store", I'd like to them "feature manager" instead because both marketplace and app store are used for en/disabling features on the platform. But NextCloud has niche apps for Android and I believe this provides more convenience to mobile users like myself.

---
## NextCloud
Since NextCloud is a file server in its core, it was the drop-in Google Drive & Photos replacement I needed. It also has built-in [WebDAV](https://en.wikipedia.org/wiki/WebDAV "Protocol for using remote file system over HTTP"), [CardDAV](https://en.wikipedia.org/wiki/CardDAV "vCard (contact info) extension for WebDAV") and [CalDAV](https://en.wikipedia.org/wiki/CalDAV "Calendar extension for WebDAV") support, which means I can use NextCloud as Google Contacts & Calendar replacement as well and access files in native file manager as if it was a USB drive 🎉️

After enabling more services from feature manager (yes, I am sticking with this name) it also became my notes, tasks, bookmarks manager as well. All powered by a VM that costs 5$/month to run, +2$ for backup.

> One who loves roses should endure thorns - Turkish Proverb

NextCloud is great. But just like every other artificial thing in this world, it isn't perfect. The biggest problem I face with it is the __performance__ of web interface. It is written in PHP and being not compiled program is not doing any favors. Image preview loading can be called sluggish by many. Since I use mobile app most of the time which caches the previews, user experience isn't bad in my opinion.

---

[Kev Quirk](https://kevq.uk "his blog") wrote a blog about his opinions and experiences with <strong title="My current choice of file server solution">NextCloud</strong> and <strong title="His choice of home server solution">Synology</strong>. This is my answer to [his blog](https://kevq.uk/synology-vs-nextcloud-which-is-better-for-a-home-server/ "Synology vs Nextcloud – Which Is Better For A Home Server?").

Synology's home server sound like a great product. I am happy for you and your family that your data is safe and accesible without giving up your privacy. After reading your blog, I wanted to try Synology as well. Upon seeing the price for [Synology 420+](https://www.newegg.com/synology-ds420/p/N82E16822108744 "4 HDD bay NAS for home/small business use (disks not included)") is 500$ and another 400$ for 4x [4TB HDD](https://www.newegg.com/seagate-ironwolf-st4000vn008-4tb/p/N82E16822179005 "SeaGate NAS HDD") for RAID 6, I believe NextCloud is the best choice I have. I am 1 student who has no movies, musics, 4K family photos or video project for YouTube channel to utilize TBs of storage not do I have budget for it. Under these requirements and constraints, I want to offer an alternative to Google to my family. Since I can't just ask for ~900$ for Synology, NextCloud on a VM is the best option I have. I still have option of increasing VM disk size or mounting external block storage as our storage needs grow.

It is nice that we have different perspectives on same topic. I wrote this answer because I wanted you to see from the eyes of a student living on pocket money and still afford for privacy of his and his family. May your Synology system last long and serve your family well 🙂️

---

If you think Google services aren't that bad and I would be better off keep using Google services, [here is my reasoning #1](https://tosdr.org/#google) and [#2](https://www.reuters.com/article/us-alphabet-google-privacy-lawsuit/google-faces-lawsuit-over-tracking-in-apps-even-when-users-opted-out-idUSKCN24F2N4 "Google faces lawsuit over tracking in apps even when users opted out - Reuters"). But if you still think that I should use Google services, tell me your reasoning and help me see your side of the coin. I would like to stay open minded.
