title: Earth 2.0
date: 2020
tags: 100DaysToOffload
summary: My vision for the new world we should build in post-Covid19 era
status: draft
comment: 
hundreddaystooffload: 

Covid19 is a virus that put the Earth on hold. People closed themselves to their houses. Those who didn't helped virus spread 😡️ Since many people listened and physically distanced themselves the spread slowed down. As a precaution schools and jobs are still in remote mode but it won't be forever. Some people really missed their cubicles 😜️



--- DRAFT ---

title: Earth 2.0

__Covid19__ disrupted economy, put borders between family members, amplified racism, got people to talk silently, __hit life like a truck.__ Now here I am, a person who stayed inside because not spreading illness is a way of caring, traveling to another city for summer holiday in last week of summer.

// Don't pity me for nearly missing the entire holiday and forgetting that there is a chance for habitability of Earth. I had good time on Fediverse and learned a few things about setting up servers. It turns out the planet continuous to rotate and natural life seems to recover without humans somehow. I will go out and swim for a while because swimming is important for health.

I believe that the Earth still has a chance for habitability. But this time lets build a society is different from Earth v1.0 in a number of ways.

1. All humans are equal  
	Let's not have discrimination. Even racist people will benefit from not being racist. Racism not only divides us in social level but also hurts our economy.
2. Cooler  
	Literally cooler ! Let's not turn life on Earth into "atmosphere is lava" game. There will be no winners.
