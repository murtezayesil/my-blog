title: About

## Ali Murteza Yesil

Mastering life through art of making mistakes.  
I make mistakes and love to learn from them. I would appreciate your constructive criticism 😅️

> Smart people learn from their experiences, geniuses however learn also from others' experiences

This kid is looking for his calling. He may have find it in advocating for privacy. This is not to say that he is perfect at defending it, he still makes rookie mistakes.

Confession: I didn't know English Language when my family and I moved to Kenya. I only learned English Language in Kenya since it was my priority, not Swahili 😐️ I still would like to call myself grew up in Kenya since this is where my personality was shaped.  
Confession: I also would like to use word "nigga" in friendly way Kenyans use, not in racist way American tend to use.

Favourite foods: Sarma (stuffed grape leaves/cabbage), matoke chips
Favourite drinks: Water, Ayran (it is NOT salty milk)
Favourite memes: [Hide the pain Harold](https://knowyourmeme.com/memes/hide-the-pain-harold), [This is fine](https://knowyourmeme.com/memes/this-is-fine)

## Blog

This blog is where Ali Murteza Yesil's opinions embody into words. This blog is compiled into static pages to reduce its carbon footprint. Instead of running webmention or disqus based comment system, I decided to share my blog posts on Mastodon and treat messages to those toots as comments. If you want to comment to my posts, we welcome you to [Fediverse](https://en.wikipedia.org/wiki/Fediverse "Learn about Fediverse on Wikipedia").

Static pages generated from markdown using [Pelican](https://blog.getpelican.com/ "Static Site Generator").

## License

Everything on this blog is licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode "Creative Commons Attribution-ShareAlike 4.0 International License") unless otherwise stated. You can find a human readable form below for quick reference. Below reference is just a summary of your rights and responsibilities. It is not the license itself.

> You are free to
>  
> > __Share__ copy and redistribute the material in any medium or format
>  
> > __Adapt__ remix, transform and build upon the material for any purpose, even commercially
>  
> Under the following terms
>  
> > __Attribution__ You must give appropriate credit, provide link to license and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
>  
> > __ShareAlike__ If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.

![CC BY-SA 4.0 badge](https://licensebuttons.net/l/by-sa/4.0/88x31.png)

## IndieWeb

I didn't implement it in this blog just yet. I will work on it next.
