title: Setup exercism CLI
date: 2021-01-18
tags: beginner, code, 100DaysToOffload
category: Notes
summary: Configuring exercism CLI and making it convenient
status: published
comment: https://fosstodon.org/@murtezayesil/105576267383642828
hundreddaystooffload: 36

[Exercism.io](https://exercism.io) has exercises for testing your programming skill. There are mentors checking students' submissions for errors and guiding them to sharpen their skills. But I think its CLI only program for downloading and uploading your code can be a turn-off for new students. I am writing this blog post to help you with easing the use of `exercism` command.

> ###Warning !
>
> I am following Rust language track on Exercism and I used it for examples.
> Remember to replace it with language track you are following.


### Installing `exercism` command

The first time you are using exercism, you should be guided on how to install it on your computer. You may find that guide [here](https://exercism.io/cli-walkthrough "Exercism CLI Walk-through"). I use Linux but don't have "snap" on my computer. I also don't want to compile it from source code. Thankfully I am using Solus and Exercism command is available in Software Center under the name `exercism-cli`. I suggest you to search "exercism" on Software Center since it can easier for you.

After installation, run `exercism version` command on terminal. If exercism was installed properly, you should see which version of exercism command is installed.


### Setting up user token

Exercism command cannot know who is downloading or submitting code without an identifying information. Every account has a token for identification, which can be found in [Exercism > Account Settings > CLI Token](https://exercism.io/my/settings). Copy it.

Next, go to terminal and type `exercism configure --token=` and then paste the token. If you find that <kbd> CTRL </kbd> + <kbd> V </kbd> not pasting the token, try <kbd> CTRL </kbd> + <kbd> Shift </kbd> + <kbd> V </kbd> or right-click on terminal window and select `Paste`.

Press enter. It may take a second or two, then you should be presented with a message similar to this:
``` sh
You have configured the Exercism command-line client:

Config dir:                       /home/$USER/.config/exercism
Token:         (-t, --token)      $TOKEN_YOU_JUST_COPY_PASTED
Workspace:     (-w, --workspace)  /home/$USER/exercism
API Base URL:  (-a, --api)        https://api.exercism.io/v1
```

Congradulations 🎉


### Changing default workspace (optional)

By default, Exercism puts all exercises into ~/exercism directory. This is fine, but I like keeping my home directory tidy. That is why I have `Projects` directory for the codes I write. I want Exercism stuff go into `~/Projects/exercism` directory instead.

`exercism configure --workspace ~/Projects/exercism` command will change its default workspace to `/home/$USER/Projects/exercism`. You will be presented with a message similar to message you received after configuring the token.


### Downloading & Submitting exercises

Command for downloading exercises is `exercism download --track rust --exercise hello-world` and submitting is `exercism submit --track rust --exercise hello-world`. But remembering these long commands can be difficult. Thankfully we have alias system in terminal which allows us to replace long commands with a short ones. With aliases I can use `exercism-down-rust hello-world` and `exercism-up-rust hello-world` instead.

``` bash
echo 'alias exercism-down-rust="exercism download --track rust --exercise "' >> ~/.bashrc
echo 'alias exercism-up-rust="exercism submit --track rust --exercise "' >> ~/.bashrc
```

From now on, you can type `exercism-down-rust hello-world` to download "hello-world" exercise or type `exercism-up-rust hello-world` to submit "hello-world" exercise. Please remember to replace language track according to your track (maybe python, java, bash, emacs-lisp etc) and exercise name according to exercise you are doing.
