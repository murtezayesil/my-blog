my_theme
========

A clean theme for Pelican inspired by [pelican-blueidea](https://github.com/nasskach/pelican-blueidea "Pelican SSG") and [Susty](https://github.com/jacklenox/susty "WordPress CMS").

Focusing on clean interface to help people read better. Reduces site load speed by __NOT__ using external fonts.

PLANNED Features
        --------
* Black Lives Matter banner.
* IndieWeb Profile
* #100DaysToOffload
* Toot for comment

* Ability to hide pages from the top menu.
* Ability to hide categories from the top menu.
* Ability to display categories as a submenu.
* Ability to sort pages using a specific attribute like the date or any additionnal attribute.
* Ability to hide the author's name or the article category in the articles info.
* Icons for the following social networks: Facebook, Github, Google+, LastFM, LinkedIN, RSS/ATOM, Twitter, Mastodon, Vimeo, YouTube, PeerTube

PLANNED Settings
        --------

	# IndieWeb Profile
	
	# Display pages list on the top menu
	DISPLAY_PAGES_ON_MENU (True)

	# Display categories list on the top menu
	DISPLAY_CATEGORIES_ON_MENU (True)

	# Display categories list as a submenu of the top menu
	DISPLAY_CATEGORIES_ON_SUBMENU (False)

	# Display the category in the article's info
	DISPLAY_CATEGORIES_ON_POSTINFO (False)

	# Display the author in the article's info
	DISPLAY_AUTHOR_ON_POSTINFO (False)

//	# Display the search form
//	DISPLAY_SEARCH_FORM (False)

	# Sort pages list by a given attribute
	PAGES_SORT_ATTRIBUTE (Title)

	# Display the "Fork me on Github" banner
	GITHUB_URL (None)

	# Blogroll
	LINKS 

	# Social widget
	SOCIAL

post front-matter
-----------------
	# \#100DaysToOffload
	hundreddaystooffload (Day Number)

	# Toot to Comment
	comment: (Link to toot)

---

---

pelican-blueidea
================

A theme for Pelican inspired by the default theme notmyidea.

Features
--------
* Ability to hide pages from the top menu.
* Ability to hide categories from the top menu.
* Ability to display categories as a submenu.
* Ability to display a search form which exploits the [DuckDuckGo](https://duckduckgo.com/) search engine.
* Ability to sort pages using a specific attribute like the date or any additionnal attribute.
* Ability to hide the author's name or the article category in the articles info.
* Icons for the following social networks: Facebook, Github, Google+, LastFM, LinkedIN, RSS/ATOM, Twitter, Vimeo and Youtube.

Preview
-------
![Screenshot-1](https://raw.github.com/blueicefield/pelican-blueidea/master/screenshot-1.png)

Settings
--------
	# Display pages list on the top menu
	DISPLAY_PAGES_ON_MENU (True)

	# Display categories list on the top menu
	DISPLAY_CATEGORIES_ON_MENU (True)

	# Display categories list as a submenu of the top menu
	DISPLAY_CATEGORIES_ON_SUBMENU (False)

	# Display the category in the article's info
	DISPLAY_CATEGORIES_ON_POSTINFO (False)

	# Display the author in the article's info
	DISPLAY_AUTHOR_ON_POSTINFO (False)

	# Display the search form
	DISPLAY_SEARCH_FORM (False)

	# Sort pages list by a given attribute
	PAGES_SORT_ATTRIBUTE (Title)

	# Display the "Fork me on Github" banner
	GITHUB_URL (None)

	# Blogroll
	LINKS 

	# Social widget
	SOCIAL

Browsers compatibility
----------------------
Only tested with latest releases of Mozilla Firefox and Chromium/Chrome.
