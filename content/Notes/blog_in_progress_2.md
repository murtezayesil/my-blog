title: Blog in Progress #2
date: 2020-08-19 12:45
tags: 100DaysToOffload
summary: I have more ambitious plans for the update to blog's theme. It will take time. I am thinking of few months.
status: published
comment: https://fosstodon.org/@murtezayesil/104715103081883094
hundreddaystooffload: 21

Current theme I use isn't all old school but it isn't responsive enough either. Since I don't want to have external dependencies to other frameworks there will be no Bootstrap import. I am thinking of writing a responsive theme from scratch. Also, I am putting more emphasize on the text size and spacing. which is one of the things I don't like about the current theme. Of course I could change text sizes in blueidea theme but I am feeling itchy for a challenge 😅️

There is currently nothing to see. It doesn't render anything since bare minimum templates such as base.html and index.html aren't in place yet.

This is an ambitious projects since I didn't write any website from scratch before. I only have basic understanding of HTML, CSS, responsive design and [a11y](https://www.a11yproject.com/ "Make Web Accessible for everyone"). Features I implemented into blueidea are held together with band-aids. That is why I didn't made a pull request with my changes to [blueidea's github repo](https://github.com/nasskach/pelican-blueidea). This new theme will have those features by design (now that is a selling pitch for something that doesn't exist yet).

Planned features are decided but not yet __TBA__

---

I still didn't fix MP4 MIME type issue. If you are facing that issue, you can download the video via . There is currently only one mp4 video and I am thinking of self hosting a PeerTube instance and embedding videos from the that instead of using limited storage space of server for this blog. 

![this is fine gif](images/gifs/this_is_fine.gif)
