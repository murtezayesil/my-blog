title: Marble League 2020
date: 2020-08-05 11:11
tags: 100DaysToOffload
summary: I am not like interested in sports. But I find Jelle's Marble Runs silly. That is a solid reason to love something 😅️
status: published
comment: https://fosstodon.org/@murtezayesil/104637973724852330
hundreddaystooffload: 14

# COVID-19, sport events and me
I am not a person who followes football, basketball or any other sports league. I don't think I was ever excited about any sports events. As a Turkish, I would support Turkey in an internetional competition. But I never painted my face red and white or went to stadium for a sport event.

I actually have not been interested in sport events for over 20 years. But ever since we had to self-isolate the [COVID-19](https://covid19.who.int/) by staying in our houses, I restarted bloging, deployed NextCloud instance for my phone and computer to sync, joined Fediverse and also started watching YouTube more and more. Some of the videos are programming related and others are entertainment, comedians and stuff.

John Oliver, host of Last Week Tonight, dedicated an episode to [sports and COVID-19's effect on them](https://www.youtube.com/watch?v=z4gBMw64aqk "Watch the episode on YouTube"). As it turns out, all sport events are halted. Except one! Marble League, Jelle's Marble Runs, continues without a hickup. I never heard about MarbleLympics before but after seeing some clips in Last Week Tonight's episode, I knew I had to watch it.

---

# [Marble League <img width=25em height=auto alt="Jelle's Marble Runs Logo" src="https://jellesmarbleruns.com/wp-content/uploads/2020/07/cropped-circle-cropped.png">](https://jellesmarbleruns.com/series/ml/)

[Jelle Bakker](https://jellesmarbleruns.fandom.com/wiki/Jelle_Bakker) co-founded Marble League with his brother [Dion Bakker](https://jellesmarbleruns.fandom.com/wiki/Dion_Bakker) back in 2014. It is a silly event and commentator speaks in a way that assumes marbles are real athletes with personalities and traits. In my opinion, it is silly in a not weird but oddly entertaining way. I ❤️ Marble League.

<table style="text-align: center;">
  <tr>
	<td width=50%>
	  <img src="https://vignette.wikia.nocookie.net/marblelympics/images/1/1d/JelleBakker.png" alt="Orange marble with purple flowers" width=75%>
	  <p>Jelle Bakker</p>
	</td>
	<td width=50%>
	  <img src="https://vignette.wikia.nocookie.net/marblelympics/images/7/78/Dion_Bakker_M1.png" alt="Black and White" width=75%>
	  <p>Dion Bakker</p>
	</td>
  </tr>
<table>

But watching a sport event without supporting any team is weird silly. I didn't like the idea of supporting whichever team is sitting on top of the leaderboard. I wanted my team to surprise marbles with its achievements and I found a team that is not expected to win, not expecting to win*, close to Earth 😉️

<div style="text-align: center;">
	<img width=50% src="https://vignette.wikia.nocookie.net/marblelympics/images/b/b1/Oceanics-0.png" alt="Orange marble with purple flowers">
</div>

* At least I am not expecting my team to win. I just like them the way they are regardless of whether they win or not. That being said, I expect them to pass balancing so that I can watch them compete.

<iframe src="https://fosstodon.org/@gray/104638372103104260/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="400" allowfullscreen="allowfullscreen"></iframe><script src="https://fosstodon.org/embed.js" async="async"></script>
