#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Ali Murteza Yesil'
SITENAME = 'Ali Murteza Yesil'
SITESUBTITLE = 'Blog'
SITEURL = 'file:///home/tui/Projects/my-blog/output'

PATH = 'content'

TIMEZONE = 'Asia/Bishkek'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
         ('Kev Quirk', 'https://kevq.uk'),
         ('Mike Stone', 'https://mikestone.me'),
         ('Yarmo Mackenbach', 'https://yarmo.eu/')
        )
#LINKS = (('Pelican', 'http://getpelican.com/'),
#         ('Python.org', 'http://python.org/'),
#         ('Jinja2', 'http://jinja.pocoo.org/'),
#         ('You can modify those links in your config file', '#'),
#        )


# Social widget
SOCIAL = (
          ('Fostodon', 'https://fosstodon.org/@murtezayesil'),
         )
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),
#         )

DEFAULT_PAGINATION = 100

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

### Some of the switches to get my ideal setup
USE_FOLDER_AS_CATEGORY = True

ARTICLE_PATHS = [
    'Notes',
    'Personal',
    'Tech',
    'draft',
]

STATIC_PATHS = [
	'extra',
    'videos',
    'images',
]

EXTRA_PATH_METADATA = {
	'extra/favicon.ico': {'path': 'favicon.ico'},
}

THEME = 'theme/put_it_out_there'

### Blueidea theme specific settings (all disabled. Testing new theme)
# Display pages list on the top menu
#DISPLAY_PAGES_ON_MENU = True

# Display categories list on the top menu
#DISPLAY_CATEGORIES_ON_MENU = False

# Display categories list as a submenu of the top menu
#DISPLAY_CATEGORIES_ON_SUBMENU = True

# Display the category in the article's info
#DISPLAY_CATEGORIES_ON_POSTINFO = True

# Display the author in the article's info
#DISPLAY_AUTHOR_ON_POSTINFO = False

# Display the search form
#DISPLAY_SEARCH_FORM = True

# Sort pages list by a given attribute
#PAGES_SORT_ATTRIBUTE (Title)

# Display the "Fork me on Github" banner
#GITHUB_URL (None)

# Black Lives Matter Banner (and border)
# this is a feature not yet implemented in to new theme and comment out temporarily
#BLACK_LIVES_MATTER = 'Absolutely'

  ##                             ##
 ##                               ##
##  put_it_out_there theme config  ##
 ##                               ##
  ##                             ##

LICENSE = "CC BY-SA 4.0"

# Whether display a menu under main menu listing categories
DISPLAY_CATEGORIES_SUBMENU = True

 ###
## ## Article Info
 ###

# article_info gives info about the article such as when it was published and what tags it carry.
# It is found below the article title in index and article pages.

# Set True to show author names in article_info if there are multiple authors on the site
# Set False if there are posts from only 1 author in this site
DISPLAY_AUTHOR_ON_ARTICLEINFO = False

# Whether display category of the article in article_info
DISPLAY_CATEGORY_ON_ARTICLEINFO = True

 ###
## ## Extra bells and whistles
 ###

# Some neat stuff this theme offers


# work-in-progress # NOW_FEATURING Banner
# This will be a banner on top of the site for something you may want people to know.
# Comment out below line to remove the NOW_FEATURING banner
FEATURING_NAME = 'Black Lives Matter'
FEATURING_LINK = 'https://blacklivesmatter.com'
FEATURING_COLOR_BG = '#000000'
FEATURING_COLOR_FG = '#fce21b'


# If you are in 512kb.club, set your badge color here
# Go to https://512kb.club/ for information on how to join :D
# Available options are "green" , "orange" , "blue"
CLUB_512KB = 'green'


## IndieWeb Integration ##
# NOTE: All info provided here will be visible to everyone on the internet. Only put what you want to be publicly available.
# Name (needed)
#	Not defined here. Instead taken from the AUTHOR variable defined earlier in this configuration file
#	Example: AUTHOR = 'John Doe'

# Biography (needed)
#	tell a little about yourself or what this blog site
BIO = ''

# Gender (optional)
#	define 1 from below list:
#		'M' for male
#		'F' for female
#		'O' for other
#		''  for don't want to say
GENDER = 'M'

# Social Profiles (encouraged but optional)
#	Add links to your social media profiles like shown in below list.
#	Actual list was declared much earlier in the config file.
#	Example syntax:
#SOCIAL = (
#  ('Mastodon', 'https://example.social/@johndoe'),
#  ('SOCIAL SITE', 'YOUR PROFILE PAGE LINK'),
#)

# Profile Picture (optional)
#	Should be placed in content/images directory. then enter picture's filename below.
#	Image may be downloaded even though indieweb section is set to be invisible
#PROFILE_PICTURE = 'avatar.png'

# Location (optional)
#	You may decide to provide city or country accurate location information
#	or avoid it for sake of privacy
#	Example: 'Dubai, UAE', 'Nairobi, Kenya', 'Japan'
#	Note: Africa is not a country
#LOCATION = 'Hecates Tholus, Mars'

# Mail (optional)
#	You may add your mail here to let other services verify your email address
#	or you may skip this to avoid announcing your email to entire world
#MAIL = 'john@smith.me'

# Pronoun (optional)
#	Define one of 'he', 'she', 'it' or 'they' as your pronoun
PRONOUN = 'he'
