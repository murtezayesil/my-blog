title: Blog in Progress #1
date: 2020-08-15 12:00
tags: 100DaysToOffload
summary: Removed Google Fonts dependency to load the page faster and discovered an issue where MP4 viewer won't load video.
status: published
comment: https://fosstodon.org/@murtezayesil/104695315322991971
hundreddaystooffload: 19

<div style="text-align: center;">
	<img src="images/gifs/construction_progress_bar.gif" alt="Under Construction GIF">
</div>

I am redesigning this blog's theme piece by piece. But there is a possibility that you will be reading this article after the new theme is applied. So [here is a reference screenshot](images/screenshots/index_full_page_screenshot_w760.webp "Old index page") before new theme is applied. Theme isn't the only change. I have few more changes in mind to extend blog's features and optimize it even further.

If you want to know how well does this page performed in GTmetrix with old theme, [here is the report](extra/gtmetrix_murtezayesil.me_2020_08_16.pdf) in PDF.

# Removing Google Font dependency
[Yanone's Kaffeesatz font](https://www.yanone.de/fonts/kaffeesatz/) is beautiful, but it takes almost 300ms to load. Even though it is as small as 20KB, it is something that is not needed.

Another reason I want to remove it is that, it is embedded through Google Fonts which also loads some Google API code. It would be ironic to not respect my readers while I am going through digital cleansing and de-Googling myself.

# MP4 videos may fail to load
Honestly, I don't know how to fix that "No video with supported format and MIME type found." error. Only future proofed solution I can think is hosting videos on PeerTube and embedding them to posts. You can download video by using "save as ..." option in context (right click) menu 😐️

Fun Fact: My IndieWeb profile image is 1024 by 1024 PNG and is 174 KB, around 9 times the Yanone's Kaffesatz font from Google Fonts, but still takes a third less time to download. That being said, it isn't displayed anywhere and shouldn't be loaded unless explicitly wanted. 128x128 would be fine too. 1024x1024 is an overkill for an image that isn't displayed 😬️

