title:	 Removing RSS feed for now
date:	 2020-12-16
status:	 published
summary: I am removing RSS feed because it doesn't include the content, only title, link and metadata. 
tags:	 100DaysToOffload
comment: 
hundreddaystooffload: 34

This blog is powered by Pelican SSG which generates pages, RSS and ATOM feeds for all the posts and ATOM feeds for each category. But I noticed that generated RSS feed doesn't include all of the text content. Subscribing to it only fetches the post title and some metadata such as summary. 

I believe full text content should be delivered with feed:

1. __No internet? No problem__: Once the feed is fetched by the user's app, no internet access required for reading it because content was already downloaded. 
2. __Listenable__: Text-To-Speech feature in feed reader apps make my site accessible to people with no sight.
3. __Better readibility__: Reader apps allow changing text size and font to make it more readable by people with weaker sight.

I want to have the perfect site that is accessible to everyone regardless of their ability, religion, nationality, internet speed etc. and I still have a long way to get there. But RSS feed not including content is a set back I can't tolerate. It has to go until I can figure out a fix. Don't worry, I am keeping ATOM feed which can be used as a replacement for RSS feed. ATOM feed delivers content.

RSS not having content is not a deficieny of RSS, but an oversight on either pelican ssg or me. I know many blogs that include content in their RSS feeds.

I will make my research on adding content to RSS but until then no RSS feed. If you know a fix for RSS on Pelican SSG, reply to toot linked below. And thanks in advance ☺
