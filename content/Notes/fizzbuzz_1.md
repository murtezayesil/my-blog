title: FizzBuzz with single semicolon
date: 2020-07-20
tags: code, 100DaysToOffload
category: Notes
summary: Attempt to write an eccentric FizzBuzz program with as few semicolon as possible.
status: published
comment: https://fosstodon.org/@murtezayesil/104552527725862495
hundreddaystooffload: 6

Semicolon is a bit special in Rust. It only comes after statements, not expressions. that includes codeblock returns. This gave me a silly idea.

## How many semicolons needed to write a FizzBuzz program in Rust?
``` rust
fn main() {
    // for every number from 1 to 100
    for number in 1..100 {
        println!(
            "{}",
            {
                // check if number is
                if number % 15 == 0 {       // divisible by both 3 & 5
                    "fizzbuzz".to_string()      // if so return "fizzbuzz"
                } else if number % 3 == 0 { // divisible by 3
                    "fizz".to_string()          // if so return "fizz"
                } else if number % 5 == 0 { // divisible by 5
                    "buzz".to_string()          // if so return "buzz"
                } else {                    // but if not
                    number.to_string()          // return number as String
                }
            }
        );  // print returned value
    }
}
```
<h1 style="text-align: center;">1</h1>

There is also [brute forcing method](https://github.com/Keith-S-Thompson/fizzbuzz-c/blob/master/fizzbuzz004.c "Brute forcing FizzBuzz in C") 🤦‍♂️️
