title: Hello, Tildeverse !
date: 2020-10-29
tags: 100DaysToOffload
summary: I joined the Tildeverse 😀️
status: published
comment: https://fosstodon.org/@murtezayesil/105119257629404285
hundreddaystooffload: 30

I first heard about Tildeverse from Tomasino's [video on PeerTube](https://peertube.dk/videos/watch/5248e160-242a-4cd4-8546-8f032f24aa61 "Probably many things are explained in this video than I could in jest text"). As technical users would know, UNIX and similar operating systems are "multi user operating systems" meaning many people can connect and use a single system at simultaneously. I already knew that from Operating Systems class in CS degree but I didn't know there where communities build around that feature. As a person fascinated by these technical stuff, I had to learn more about it and maybe even join.

<div style="text-align: center;">
	<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://peertube.dk/videos/embed/5248e160-242a-4cd4-8546-8f032f24aa61" frameborder="0" allowfullscreen></iframe>

</div>

# ~

Many technical people in IT space would know that tilde sign (~) is an synonym for home directory in UNIX based and similar systems. My personal directory on my computer is __/home/murteza__ and same folder is accessed when I open the __~__ folder. So my musics in __~/Musics__ are actually in __/home/murteza/Musics__ folder.

# Tildeverse

Tildeverse is a community of people sharing resources of the same servers. Some servers have hundreds of users, thus hundreds of tildes since each user must has a personal home directory. People login and logout all the time and there are times when over 30 people connected to a single server.

---

You may be wondering why would that many people share a single computer. After all, people are connecting to their ~ on Tildeverse using their own computers anyway.

# So many services for the community

Tildeverse offers many services free of charge to everyone in Tildeverse. Some services are even usable without having a ~ in tildeverse. Tinkering, learning and sharing is highly encouraged and there are many services being offered to increase communication and collaboration. It definitely caught to my attentions that most of the services on Tildeverse are about communication. Tildeverse isn't a company selling those services or an NGO trying to solve no access to those resources isssue. Instead Tildeverse is a home to hackers, musicians, authors, artists and many more that share their knowledge, creativity (and opinions) and ask questions.

All those services enable the community members to do what they want or need to do.

# It is all about the community

This only enforces what was previously mentioned. All those services are for the community. Without the community, services on Tildeverse would be meaningless or not even offered.
