title: Digital Cleansing - Identifying services we use
date: 2020-07-14 03:40
tags: digitalcleansing, privacy, 100daystooffload
summary: Step 1 of digital cleansing is identifying services I want to drop
status: published
comment: https://fosstodon.org/@murtezayesil/104509732236403886
hundreddaystooffload: 3

# Digital Cleansing For Better Privacy
1. *Identifying products and services we use*
2. Finding/Offering alternatives
3. Moving to alternatives and helping my family to move as well

I will make a list of services I use or used in the past but didn't delete my account. For each service, I will look for alternatives, move my data to alternatives or deploy an instance of alternative on my server. I will use the alternatives for a while and learn more about how to use them properly. This way I will be able to help my family have a smooth transition. 

---

Here is my list. This list may be different for you, therefore you should make your own list.  
This list may get longer as I remember services I subscribed to but didn't use in a while.

1. Gmail
2. Google Drive
3. Google Photos
4. Google Contacts
5. Google Search
6. Facebook
7. Instagram
8. WhatsApp
9. WhatsApp (Voice/Video Call)
10. Zoom
11. Windows OS
12. Stock Android ROM (due to embedded Google services)
13. Mi account (because I used MIUI Android ROM)
14. ...
15. Note to self: Check archieved emails to find services you stopped using and request account deletion

---

Now I have a "list of service to opt-out from" to help me focus better. That being said, I started to de-google a while ago and replaced Gmail with Protonmail already. If you think Google is privacy friendly and no need to avoid Google services, [here is why I decided to de-google](https://tosdr.org/#google). 
I will start with looking for alternatives to Google Drive.


If there is any other service you think I should stay away from, you can write to comment toot.  
If you have written a blog post as an answer, mention that too.  
If you think some of the services or softwares I mentioned here aren't that bad and I would be better of keep using them, please share why you think so. I want to keep an open mind and look at those services from your perspective too.

