title: Digital Cleansing For Better Privacy
date: 2020-07-12 00:07
tags: digitalcleansing, privacy, 100DaysToOffload
summary: I am documenting my journey to claiming my digital freedom. Previously called "My Master Plan For Privacy (of my family)".
status: published
comment: https://fosstodon.org/@murtezayesil/104498776573654126
hundreddaystooffload: 2

I previously wrote about [how I got became more privacy caring individual](privacy_for_the_whole_family.md "Privacy For The Whole Family") and I tooted about [My Master Plan for Privacy of My Family](https://fosstodon.org/@murtezayesil/104480280886518081). As I grew up, I came to realize how much we gave up on privacy for the convinience and "free" services. We are social creatures. I was using Google Photos, WhatsApp, Youtube and Instagram as much as I could and my family is doing the same. We are putting each other's privacy at stake by uploading data about each other without knowing. I decided to change that either by finding privacy focused alternatives to digital services I was using or by build server system to offer alternatives myself.

I am not the first to do this. There are many privacy friendly alternatives developed by people who care about privacy. It isn't hard to find those [alternatives](https://alternativeto.net/ "Crowdsourced Software Recomendations"). Many people went through this journey, which I call "Digital Cleansing For Better Privacy". During my journey, I will document the steps I have taken and write my thoughts about the alternatives I tried.

---

# Digital Cleansing For Better Privacy
1. Identifying products and services we use
2. Finding/Offering alternatives
3. Moving to alternatives and helping my family to move as well

---

After identifying the products and services that needs replacing, I will loop step 2 and 3 for each product and service. This way I will be introducing only 1 service to my family at a time. I am planning to give them enough time for learning each alternative and understand why they should use it instead. Else, this would get overwhelming very quick.

