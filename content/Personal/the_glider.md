title: The Glider
date: 2020-10-07 23:11
tags: 100DaysToOffload
summary: Yes, I embrace the hacker culture. Here is an emblem ...
status: published
comment: https://fosstodon.org/@murtezayesil/104995043753763292
hundreddaystooffload: 29

When I wanted to cleanse myself from big tech and become a more independent person, I found myself watching tutorials and reading guides on how to deploy a NextCloud instance and host Jitsi service on an owned system. This desire to have my own systems pushed me into learning about server side of the Linux. Today I would like to call myself a novice system administrator that manages 1 or 2 virtual machines at a time.

Learning about server side made me more aware of what I actually am using while on the web. I got curious and learned about what my computer says to ISP when I type [eff.org](https://eff.org "Electronic Frontier Foundation"). I can imagine what my computer says to Minecraft server when I break or place a block in the virtual world. And computers aren't "magick" to me anymore but fascinating tools that enables one in some way. This is where hacker life style comes in.

What makes computers so powerful is the ability to program them. With power of programming, that beautiful pile of plastic, metal and sand becomes clay to be shaped into any shape we want. In a way, programming is modifying. And hacking is tinkering. At least this is how I see it. This is why hacking is interesting to me. I enjoyed mixing and shaping play-doughs when I was a little child and now I enjoy molding my computer and server into what I need.

I see myself as a hacker. Not the kind that you see in movies, I am not interested in your credit card or data, or utilities like power plant. I would rather safely toy within my comfort zone, inside my LAN network that I appropriately named "Get off my LAN".

<div style="text-align: center;">
	<a href='https://web.archive.org/web/20080228153136/http://www.catb.org/hacker-emblem/'>
		<img src='https://web.archive.org/web/20080217202658im_/http://www.catb.org/hacker-emblem/glider.png' alt='hacker emblem' />
	</a>
	<p>
		<a href='https://en.wikipedia.org/wiki/Glider_(Conway%27s_Life)'>The Glider</a>, <a href='https://web.archive.org/web/20080228153136/http://www.catb.org/hacker-emblem/'>hacker emblem</a>
	</p>
</div>


