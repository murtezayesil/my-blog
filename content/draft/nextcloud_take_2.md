title: NextCloud - Take 2
date: 2020-01-01
tags: nextcloud, 100DaysToOffload
summary: I made noob mistakes with my first NextCloud server setup. So I am attempting again with my new knowledge.
status: draft
comment: 
hundreddaystooffload: 

I have done many noob mistakes while deploying the NextCloud server. It was a great experience and I learned alot during the process. Now that I feel more comfortable with CLI, SSH and few more jargon, it is time for another attempt to deploy NextCloud properly.
