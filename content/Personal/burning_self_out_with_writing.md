title: Burning self out with writing
date: 2020-11-29
tags: 100DaysToOffload
summary: I felt burned out and was thinking of giving up on #100DaysToOffload entirely
status: published
comment: https://fosstodon.org/@murtezayesil/105295301353917868
hundreddaystooffload: 31 (not last day)

I started blogging in March 2020 (I was using MDBook to generate documentation like static sites). When I was moving to Pelican SSG, I also wanted to change the style of my writing to producer higher quality articles. That was little after [Kev Quirk announced #100DaysToOffload](https://kevq.uk/100-days-to-offload/) concept. I thought that was a good time to participate in _[100DaysToOffload](https://100daystooffload.com/)_.

There is nothing wrong with _100DaysToOffload_. Problem was in my execution. I both wanted to write better and more frequently at the same time while still being a newbie at blogging. Spending around 8 hours a day on a blog post and thinking about what to write all the time isn't sustainable for me.

> The point of #100DaysToOffload is to be a relaxing and cathartic experience. Not a worrisome affair where you’re thinking of things to write all the time.  
> Posts don’t need to be long-form, deep, meaningful, or even that well written. ... What’s important is that you’re writing about the things you want to write about.  
> \- [100DaysToOffload](https://100daystooffload.com/)



You see where my problem is! I just liked the idea of being part of a group that is consistent at something that I wasn't ready for. To be frank, I wrote. Sometimes on time, sometimes with delay. But I wrote. What hurts me is I feel like I gave up on my integrity whenever I postponed. I feel like I broke the promise I made even though nobody else asked for it. __I finally burned myself out__ by thinking too much on this and putting mental pressure on me for writing more.

Now I am writing about possibly giving up entirely even though I don't want to. Maybe I should postpone it to a time when I feel better at writing more frequently. Or maybe I should write time to time without caring about how frequent the posts are and still tag them _#100DaysToOffload_ because they will truly be on days I Offloaded.

Yes, I failed. I started writing this post with anger. As one usually does while writing, I started see more clearly. This is a growing pain and there is a lesson for me. I may not be happy for writing admitting it but I am glad for learning something.

---

Note:
I made my mind. This will be take some break and return to writing at some point. There will very likely be varying amount of gaps between articles. I won't care. I only will write when I feel the need for it. Without burning myself out. About numbering though, Should I restart from 1 or continue with 32?
