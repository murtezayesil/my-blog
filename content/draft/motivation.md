title: Motivation
date: 2020-09-01 21:40
tags: 100DaysToOffload
summary: Staying motivated is a problem Mike, I and many more people share.
status: published
comment: https://fosstodon.org/@murtezayesil/104790571054679266
hundreddaystooffload: 25

Motivation is that magic like force that keeps us going through life. One can't have enough of it. Problem is that it is decays, expires, cannot be chased or collected. It comes and goes seemingly randomly. At least that is how I learned to see motivation as. Sorry for sounding pessimistic but it is hard to say good things when you don't have motivation. 

[Mike Stone](https://fosstodon.org/@mike) wrote about his struggle to find [motivation](https://mikestone.me/motivation) back in June. When I first joined Fosstodon and was a much more amateur in life, I saw Mike and Kev as role models in IT. Now seeing that Kev had problem keeping up with [#100DaysToOffload](https://100daystooffload.com/) that he started in the first place and Mike is also facing lack of motivation too, I am relived 😐️

I worried about something being inherently wrong with me. It turns out motivation comes and goes. There maybe a way to find motivation but I come to believe that the __way to find motivation is different for everybody, time and occasion__.

For many occasions the motivation for Mike was his family. Me writing this post instead of postponing it for 1 more day is the fear of feeling shame and at loss.

<div class="tooltip">Hover or tap to read removed Draft Notes
  <span class="tooltiptext">Sometimes motivation wears a mask called "purpose" and my eyes just miss it, when I don't see the purpose in something I just don't have motivation to do it. My wish is the motivation being apparent to all of us so that we don't waste time.</span>
</div>
