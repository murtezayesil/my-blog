title: Digital Cleansing - Mastodon
date: 2020-07-24 15:23
tags: digitalcleansing, privacy, fediverse, 100DaysToOffload
summary: Centralized microblogging platforms are rich in user data and attractive to cyber criminals. I recommend decentralized alternatives.
status: published
comment: https://fosstodon.org/@murtezayesil/104570041953140255
hundreddaystooffload: 8

## Microblogging
[Microblogging](https://en.wikipedia.org/wiki/Microblogging "Read in more details on Wikipedia") is blogging smaller but usually more frequent updates. Microblogging platforms put some limitaions like the number of characters, photo size and video length in their platforms. Constraints make people to get creative to craft short and brief posts.  
These platforms take away the hustle of maintaining your blog and reduce the friction to start writing/sharing.

Do you know someone who uses a microblogging platform?  
I guess you do. I will go as far as saying that may be you are using a microblogging platform.  
Surprised? Don't be.  
Reddit, Twitter, Facebook, Instagram, Tumblr, VK, Fediverse are just a few.

Every microblogging platform implements it differently.  
Twitter is a place where everybody can see everybody else's posts.  
Facebook is for only reading posts from people you know in real life.  
Instagram is for posting photos and short videos.  
Tumblr is for normal blogging first and microblogging in form of comments.  
Reddit is similar to Tumblr except that it is more categorized and structured imo.

All of the platforms I just mentioned, except Fediverse, has something in common. __They are centralized__. Centralized means aech of these services have their own data centers where they aggregate credentials and data of their users. These data centers are rich in user data and appealing target for cyber criminals. Adding fuel to the fire, some of those companies put weak cyber security systems in place and couldn't very large data breaches.

### Fediverse is different

1. Fediverse is decentralized. There are many small servers (aka instances) communicating with each other. They collectively form Fediverse.

2. Instances forming the Fediverse can run as different platforms. Therefore Fediverse isn't only about microblogging.

---

Even though I said Fediverse is different from other microblogging platforms, threat of cyber attacks are very possible. Every instance carries user credentials and user data of its own users. If an instance was to be cyber attacked, only the users of that particular instance would be affected while rest of Fediverse function unaffected.

---

## Mastodon

I heard about Mastodon in a [IRL Podcast](https://irlpodcast.org/season4/episode6/ "Decentralize It - S4/E6"). I just wanted to try that twitter like thing that somehow didn't have a central place to collect all the tweets everybody was posting. I created an account on some instance and tried it for few days.  
What I understood at the time was that, there are servers running Mastodon system and Mastodon systems on different servers can communicate with each other to inform about cross platform messages. Fediverse is formed when instances start sommunicating with each other.

<div style="text-align: center;">
    <iframe id='ivplayer' width='640' height='360' src='https://invidio.us/embed/IPSbNdBmWKE?autoplay=0' style='border:none;'></iframe>
</div>

For example: I am @murteza on strawberry.garden instance and I need help to collect strawberries. When I post "We need @batman@gotham.city to help us collect strawberries🍓️", @batman from gotham.city instance will receive a notification from @murteza@strawberry.garden.  

---
# Deploying a Mastodon instance for my family
I want to become a SysAdmin and know all about this server stuff. So I decided to deploy servers that I will need to maintain and learn about system administration during the process. I believe this is a good excuse to stay indoors during pandemic 😷️ (Emoji doesn't imply that I am sick, I am not. I also don't want to become one)

I deployed a NextCloud server for my family and a Jitsi server too. Why not add Mastodon to do pile as well. I rented a 5$/month server and a 100GB storage for database and user uploaded content for another 5$/month.

[Mastodon's documentation](https://docs.joinmastodon.org/ "What is Mastodon?")'s installation guide part expects a SysAdmin to know more than I do. I also got help from [another guide](https://computingforgeeks.com/install-mastodon-on-ubuntu-with-letsencrypt-ssl-certificate/ "Install Mastodon on Ubuntu 20.04/18.04 With Let’s Encrypt SSL Certificate"). But after try installing Mastodon twice and rollback once, [I got a working instance](https://yesil.club/). Since it is an instance for my family. I am not allowing account creation from outside, but only via invite.

Next piece of the puzzle will be backuping database and user data. I don't know what is the best way to approach this but we shall see.
