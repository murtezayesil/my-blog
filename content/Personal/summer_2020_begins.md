title: Summer 2020 Begins
date: 2020-08-28 22:09
tags: 100DaysToOffload
summary: It is nearly end of August and it finally is safe enough to go out.
status: published
comment: https://fosstodon.org/@murtezayesil/104768112731738291
hundreddaystooffload: 24

It is nearly end of August and it finally is safe enough to go out for a holiday. In fact, I am back. I spend last 4 days in rural area near a lake. When I first received the invite, I didn't want to go. I was making excuses like "clinics in rural area may not have adequate tools for Covid19" in my head. As it turns out, there isn't many people on country side and it would be an achievement to get sick there.

2 things to convinced me go to:

- Seeing that Covid19 cases in city was record low. That means going to country side maybe safe enough.
- I thought to myself "Elderly people often talk about their regrets and their regrets usually are the things they didn't do. If I miss on that, I may regret"

I also was away from computer (almost) the whole time. For sake of consistency, I wanted to write a blog about my time there. But given that I take my time to write, I could spend an entire day in front of a computer. Instead I postponed it to after return which is now.

## Conclusion
I am glad I did go.  
I am glad I didn't spend my time there in front of a computer.  
I am glad 😀️
