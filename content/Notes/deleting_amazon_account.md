title: Deleting Amazon account
date: 2020-07-26 21:15
tags: 100DaysToOffload
summary: Deleting Amazon account has more steps (friction) than I would like. But not impossible.
status: published
comment: https://fosstodon.org/@murtezayesil/104581385751399042
hundreddaystooffload: 9

I call it "deleting account", Amazon calls it "closing account". I used them interchangibly in this post.

If you set your mind to delete (or close) your account, don't let too many steps discourage you. I understand that some people run their businesses on AWS and deleting their Amazon account will also delete AWS servers connected to that account. Amazon has given me 2 warnings about it and accepted to delete my account anyway. I wasn't invested in Amazon ecosystem and never used AWS anyway.

According my experience, here are the steps you may need to go through:

1. Click on [Help](https://www.amazon.com/gp/help/customer/display.html "Currently this page") in the page footer and search for "close account".
2. You will be presented with an article that warns you about Amazon services you will lose access to. Read and make sure you stopped using those services already.
3. There is "__Note:__ To close your account, please [Contact Us](https://www.amazon.com/hz/contact-us/request-data) to request that your account be closed." at the very end of the help page. Click on "Contact Us" to continue.
4. If not yet logged in you will be asked to, login. Choose email for communication. There is also an option to call but I don't know how would that go since I don't live in US or Canada.
5. They will send the same help article with a bit more information and links to make sure you stopped using everything such as AWS, Alexa, Kindle Publishing, Prime video, Amazon Music etc. Near the end of email you will see "If you still want to close your Amazon.com account after reviewing the items above, please click this link and state that you want to close your account: *Some Link*", click on the link.
6. It will open a page with a text box. Write why you want to close your account and submit. If you are not sure what to write and also feeling adventurous, you can copy paste this link : https://www.youtube.com/watch?v=dQw4w9WgXcQ
7. They will email you within 24 hours either for more instructions or to tell you that your account is closed.

__Note__: Depending on where you live or how many Amazon services you used, you may need to go through more steps.
