title: Rust Development on Solus - 1
date: 2021-01-16
tags: 100DaysToOffload, development
summary: A Note to self for preparing Solus for Rust development
status: published
comment: https://fosstodon.org/@murtezayesil/105564665297641023
hundreddaystooffload: 35

I recently started the courses on [Exercism](https://exercism.io) to learn Rust language by practice. But I suddenly hit the first road block which was the missing `cc linker`, a glue that puts compiled binaries together.

```sh
$ cargo run
Compiling hello_world v0.1.0 (~/Projects/exercism/hello_world)
error: linker `cc` not found
  |
  = note: No such file or directory (os error 2)

error: aborting due to previous error

error: could not compile `hello_world`

To learn more, run the command again with --verbose.
```

After some Ducking I came across an answer on [StackOverflow](https://stackoverflow.com/questions/52445961/how-do-i-fix-the-rust-error-linker-cc-not-found-for-debian-on-windows-10#52445962) and learned that there is a package called `build-essentials` for Ubuntu that installs basic stuff programmers need. But I am on Solus which is not based on Ubuntu. I learned from [another answer](https://stackoverflow.com/questions/37519076/how-to-solve-repo-item-build-essential-not-found-in-solus) that `system.devel` component on Solus offers the same convenience. But since it is a component, not a package, we need to run `sudo eopkg install -c system.devel` command to install it.

```sh
$ cargo run
    Finished dev [unoptimized + debuginfo] target(s) in 0.02s
     Running `target/debug/hello_world`
Hello, world!
```

I know I will do something to mess up my system in future and I will seek this information again. I hope this information helps you too.
