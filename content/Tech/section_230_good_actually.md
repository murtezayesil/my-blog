title: Section 230 is Good, Actually
date: 2020-12-11
tags: 100DaysToOffload, 
summary: CDA 230: the most important law protecting the internet
status: published
comment: https://fosstodon.org/@murtezayesil/105361361466203857
hundreddaystooffload: 32

I read Jason Kelley's post on EFF's site after [Doug Belshaw](https://fosstodon.org/@dajbelshaw) shared it on FOSStodon. I really liked it and recommend you to read it too.

### [Section 230 is Good, Actually by Jason Kelley](https://www.eff.org/deeplinks/2020/12/section-230-good-actually)
