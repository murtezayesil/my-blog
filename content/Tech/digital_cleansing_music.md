title: Digital Cleansing - Music
date: 2020-08-03 15:00
tags: digitalcleansing, music, 100DaysToOffload
summary: There aren't many options to buy DRM free music, are there!
status: published
comment: https://fosstodon.org/@murtezayesil/104626744957539379
hundreddaystooffload: 13

I like listening to musics and I am not the only one. Just look at how many people are listening to [LoFi music with the study girl](https://www.youtube.com/watch?v=5qap5aO4i9A "lofi hip hop radio - beats to relax/study to on YouTube") or look how many views [this song](https://www.youtube.com/watch?v=dQw4w9WgXcQ) has. As a young adult, I remember the times the CDs were the king of music distribution. But they are long gone. Thanks to internet, digital music stores and music streaming services replaced CDs.  
So here I am, looking for the best music listening solution I can get. After a little search on the internet I came up with the below list of music stores or streaming services.

Online Music Stores | Music Streaming Services
---|---
[iTunes](https://www.apple.com/itunes/ "DRM") | [Apple Music](https://music.apple.com/ "DRM")
[BandCamp](https://bandcamp.com/) | [BandCamp](https://bandcamp.com/)
[Amazon Store](https://amazon.com "DRM") | [Amazon Music](https://music.amazon.com/ "DRM")
[eMusic](https://www.emusic.com/ "Limited selection but may use") | [Spotify](https://www.spotify.com/ "Not available")
[Bleep](https://bleep.com/ "limited selection") | [Napster](https://us.napster.com/ "DRM")
[Google Play Music](https://killedbygoogle.com/ "Killed By Google - December 2020") | [YouTube Music](https://music.youtube.com/ "Not available")
[Jamendo](https://www.jamendo.com/) | [Jamendo](https://www.jamendo.com/)
[BeatPort](https://www.beatport.com/ "not my jazz") | [Tidal](https://tidal.com/ "DRM")

There are many options and above isn't an exhaustive list. Thankfully I have constraints to help me make more educated choice.

1. __Availability__ (in my region) : Even though I can use VPN to appear on some other location on the face of Earth, my bank card will give my location up or won't be accepted 👎️
2. __DRM__ : I want to put music on my phone, computer and even my NextCloud to listen from anywhere. When I spent money on music, I don't want to stuck with whatever the vendor supports. I want no vendor lock-in or any other limitation. Those limitations are pushing many people to pirate 😥️
3. __Discoverability__ : Whenever there is a new music, I want to be able to find it.
4. __Options__ : Does it even has my jazz!

# Finally here are my options

Online music stores | Streaming services
---|---
[BandCamp](https://bandcamp.com/)   | [Jamendo](https://www.jamendo.com/)
[Jamendo](https://www.jamendo.com/) | [BandCamp](https://bandcamp.com/)

## Why BandCamp
DRM free songs and musics to purchase and download in high quality. I skimmed over the Term of Services and I dind't notice any red flags for my privacy. It isn't the perfect service. Every artist isn't there and only some of the new songs appear there. But its ToS and Privacy Policy is one of the best ones I skimmed through in a long time.

## Why Jamendo
Musics licensed under Creative Commons licenses\* are available to listen without an account and available to download in high quality with an account. There almost isn't any popular song on Jamendo. But that is an oppportunity to discover new talents. It also has a nice ToS and Privacy Policy.

### I love DRM-free stuff
As a student living on pocket money, I don't have much of a budget for purchasing music. I am planning to spend 5$ every month on DRM-free musics instead of a streaming service with DRM. I like DRMless content because I know vendor is trusting me. Whenever a platform emposes DRM, I feel treated like a pirate who is expected to steal. No thanks to digital content pirates, I understand why a vendor would put DRM and it is sad that this is the reality. I just am happy that there still are DRM-free alternatives 🙃️

---

## Why not pirate <img height="25em" width="auto" src="https://fosstodon.b-cdn.net/custom_emojis/images/000/083/931/original/a43b334e1ab006d2.png" alt="funny shaped thinking emoji">
I don't believe pirating is ethical. Someone spent hours coming up with a poem or lyrics, someone else composed music for it, and someone else (probably) drank raw eggs to maintain her/his voice. I wouldn't want people get my hardwork for free unless I put it out there as a demo or as open source. Some people act like pirating is okay if the singer or band is rich. It isn't okay to pirate and I am not giving up on my principles to enjoy some music for free.

---

\* Most of the musics on Jamendo are licensed under Non Commercial licenses, such as [CC BY-NC-SA 4.0](https://www.jamendo.com/legal/creative-commons "read a little description here"). You need to purchase a commercial license to play them in your restaurant, bar, cafe, clothing store, convenience store, market, shopping mall, advertisement, podcast, YouTube/Vimeo/PeerTube video, movie, etc.
You can't play songs from your Spotify account either. Songs on Spotify/Apple Music/YouTube Music and other popular services are for personal listening only too, unless they specifically mention that it is okay to play a song in a venue in music's license. Pijama parties are okay I guess.
